defmodule VisionsUniteWeb.Layouts do
  use VisionsUniteWeb, :html

  embed_templates "layouts/*"
end
