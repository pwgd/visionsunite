defmodule VisionsUniteWeb.Router do
  use VisionsUniteWeb, :router

  import VisionsUniteWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {VisionsUniteWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  pipeline :user_session do
    plug VisionsUniteWeb.UserSessionPlug
  end


  ## Authentication routes

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    live_session :redirect_if_user_is_authenticated,
      on_mount: [{VisionsUniteWeb.UserAuth, :redirect_if_user_is_authenticated}] do
      live "/users/register", UserRegistrationLive, :new
      live "/users/log_in", UserLoginLive, :new
      live "/users/reset_password", UserForgotPasswordLive, :new
      live "/users/reset_password/:token", UserResetPasswordLive, :edit
    end

    post "/users/log_in", UserSessionController, :create
  end

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :require_authenticated_user,
      on_mount: [{VisionsUniteWeb.UserAuth, :ensure_authenticated}] do
      live "/users/settings", UserSettingsLive, :edit
      live "/users/settings/confirm_email/:token", UserSettingsLive, :confirm_email
    end
  end

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :require_authenticated_user, :require_confirmed_user]

    live_session :require_confirmed_user,
      on_mount: [{VisionsUniteWeb.UserAuth, :ensure_authenticated}] do
      live "/groups/new", GroupLive.NewGroup, :index
      live "/messages/new/:group_id", MessageLive.NewMessage, :index
    end
  end

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :user_session]

    get "/all_groups", GroupController, :all_groups
    get "/all_messages", MessageController, :all_messages
    get "/about", PageController, :about
    post "/about", PageController, :update_about

    get "/show/:group_id", GroupController, :show_group
    get "/show_all/:group_id", GroupController, :show_all_in_group
    get "/show_my_proposed/:group_id", GroupController, :show_my_unvetted_in_group

    get "/topic/:topic_id", MessageController, :messages_in_topic
    get "/topic_all/:topic_id", MessageController, :all_messages_in_topic

    get "/", GroupController, :all_groups
  end

  # Other scopes may use custom stacks.
  # scope "/api", VisionsUniteWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: VisionsUniteWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  ## Authentication routes

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    post "/users/register", UserRegistrationController, :create
    post "/users/reset_password", UserResetPasswordController, :create
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :require_authenticated_user, :user_session]

    put "/users/settings", UserSettingsController, :update

    post "/subscribe/:group_id", SubscriptionController, :subscribe
    delete "/unsubscribe/:group_id", SubscriptionController, :unsubscribe

    get "/requesting_vetting", VettingRequestController, :requesting_vetting
    get "/vettings/message/:message_id", VettingController, :new_message_vetting
    get "/vettings/group/:group_id", VettingController, :new_group_vetting
    post "/vettings/allow", VettingController, :allow
    post "/vettings/reject", VettingController, :reject

    get "/requesting_support", SupportRequestController, :requesting_support
    get "/supports/:message_id", SupportController, :new
    post "/supports/approve", SupportController, :approve
    post "/supports/reject", SupportController, :reject

    get "/my_proposed_groups", GroupController, :show_my_unvetted_groups
    get "/joined_groups", GroupController, :joined_groups
    get "/groups/show/:group_id", GroupController, :show_group
    get "/joined_messages", MessageController, :joined_messages
    get "/my_authored_messages", MessageController, :my_authored_messages

    get "/new_notifications", NewNotificationController, :new_notifications
    get "/unresponsive", NewNotificationController, :unresponsive
    get "/mark_responsive", NewNotificationController, :mark_responsive
    get "/mark_group_as_read/:group_id", NewNotificationController, :mark_group_as_read
    get "/goto_group_mark_read/:group_id", NewNotificationController, :go_to_group_and_mark_read
    get "/mark_message_as_read/:message_id", NewNotificationController, :mark_message_as_read
    get "/goto_topic_mark_message_read/:topic_id/:message_id", NewNotificationController, :go_to_topic_mark_message_read

    resources "/groups", GroupController
    resources "/messages", MessageController
  end

  scope "/", VisionsUniteWeb do
    pipe_through [:browser, :user_session]

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end
end
