defmodule VisionsUniteWeb.MessageLive.NewMessage do
  use VisionsUniteWeb, :live_view

  alias VisionsUnite.Groups
  alias VisionsUnite.MessageDrafts
  alias VisionsUnite.Messages
  alias VisionsUnite.Topics

  @impl true
  def render(assigns) do
    ~H"""
    <div class="container">
      <.header>
        New Message for <%= @group.name %>
      </.header>

      <.simple_form
        for={@form}
        id="message-form"
        phx-submit="save"
        phx-change="check_existing"
      >
        <.input field={@form[:topic_text]} type="text" label="Topic" value={@topic} phx-debounce="500" phx-value-group_id={@group.id} required />
        <div :if={!Enum.empty?(@matched_topics)}>
          <p>Here are some topics with similar names. Perhaps consider posting to one of these topics instead of creating a new one.</p>
          <ul>
            <li class="matched" phx-click="insert_topic" phx-value-topic={matched_topic.name} :for={matched_topic <- @matched_topics} ><%= matched_topic.name %></li>
          </ul>
        </div>

        <br/>

        <.input field={@form[:message_text]} type="textarea" label="Text" value={@text} phx-debounce="500" required />

        <br/>

        <.input field={@form[:group_id]} type="hidden" value={@group.id} />
        <.input field={@form[:author_id]} type="hidden" value={@current_user.id} />

        <:actions>
          <.button phx-disable-with="Saving...">Save</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def mount(%{"group_id" => group_id}, _params, socket) do
    group = Groups.get_group!(group_id)
    topics = Topics.list_topics_for_group(group_id)
    draft = MessageDrafts.get_draft_for_user_and_group(socket.assigns.current_user.id, group_id)
    user_has_pending_groups = Groups.user_has_pending_groups(socket.assigns.current_user)

    name = if !is_nil(draft) and !is_nil(draft.topic_text) do
      draft.topic_text
    else
      ""
    end

    matched_topics = if !is_nil(draft) and name !== "" do
      topics
      |> Enum.filter(fn topic ->
        String.contains?(String.downcase(topic.name), String.downcase(name))
      end)
    else
      []
    end

    socket =
      socket
      |> assign(:draft, draft)
      |> assign(:text, if !is_nil(draft) do draft.message_text else "" end)
      |> assign(:topic, if !is_nil(draft) and !is_nil(draft.topic_text) do draft.topic_text else "" end)
      |> assign(:topics, topics)
      |> assign(:matched_topics, matched_topics)
      |> assign(:group, group)
      |> assign(:user_has_pending_groups, user_has_pending_groups)
      |> assign(form: to_form(%{}, as: "message"))

    {:ok, socket}
  end

  @impl true
  def handle_event("insert_topic", %{"topic" => topic}, socket) do
    socket =
      socket
      |> assign(:topic, topic)
      |> assign(:matched_topics, [])

    {:noreply, socket}
  end

  @impl true
  def handle_event("check_existing", %{"message" => message_params}, socket) do
    topic_text = message_params["topic_text"]

    message_text = message_params["message_text"]
    same = if Map.has_key?(socket.assigns, :topic_text) do
      socket.assigns.topic_text
    else
      nil
    end


    matched_topics =
      cond do
        topic_text == same ->
          []
        topic_text == "" ->
          []
        true ->
          socket.assigns.topics
          |> Enum.filter(fn topic ->
            String.contains?(String.downcase(topic.name), String.downcase(topic_text))
          end)
      end


    socket =
      socket
      |> save_draft(message_params)
      |> assign(:matched_topics, matched_topics)
      |> assign(:message_text, message_text)
      |> assign(:topic_text, topic_text)

    {:noreply, socket}
  end

  def handle_event("save", %{"message" => message_params}, socket) do
    group_id = message_params["group_id"]
    topic_text = message_params["topic_text"]
    topic = Topics.get_topic_by_group_and_name(group_id, topic_text)
    topic = case topic do
      nil ->
        {:ok, topic} =
          Topics.create_topic(%{
            name: topic_text,
            group_id: group_id
          })
        topic

      existing_topic ->
        existing_topic
    end

    message = %{
      "topic_id" => topic.id,
      "text" => message_params["message_text"],
      "author_id" => message_params["author_id"],
      "group_id" => message_params["group_id"],
    }

    case Messages.finalize_message_and_request_vetting(message) do
      {:ok, _message} ->
        group = Groups.get_group!(message["group_id"])
        # The message draft is deleted in /lib/visions_unite/messages.ex
        {:noreply,
         socket
         |> put_flash(:info, "Message created successfully.  It will be visible to others once it is assessed not to be spam by your peers, and will be sent to everybody in the group if judged to be especially important!")
         |> redirect(to: ~p"/show/#{group.id}")}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  def save_draft(socket, message_params) do
    # check that socket.assigns.draft is not nil
    IO.puts("saving draft==================")

    socket =
      if is_nil(socket.assigns.draft) do
        # if it is, save a new message
        {:ok, draft} =
          MessageDrafts.create_draft(message_params)

        socket
        |> assign(:draft, draft)
      else
        # if it is not, we update that draft
        MessageDrafts.update_draft(socket.assigns.draft, message_params)

        socket
      end

      socket
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end
end
