defmodule VisionsUniteWeb.UserRegistrationLive do
  use VisionsUniteWeb, :live_view

  alias VisionsUnite.Accounts
  alias VisionsUnite.Accounts.User

  def render(assigns) do
    ~H"""
    <main class="container">
      <.header class="text-center">
        Register for an account
        <:subtitle>
          Already registered?
          <.link navigate={~p"/users/log_in"} class="font-semibold text-brand hover:underline">
            Sign in
          </.link>
          to your account now.
        </:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="registration_form"
        class={[
          if(@check_errors, do: "has-errors", else: "no-errors"),
        ]}
        phx-submit="save"
        phx-change="validate"
        phx-trigger-action={@trigger_submit}
        action={~p"/users/log_in?_action=registered"}
        method="post"
      >
        <p class="alert alert-warning" :if={@check_errors}>
          ⚠️ Oops, something went wrong! Please check the errors below.
        </p>

        <.input field={@form[:email]} type="email" label="Email" required />
        <p>By registering with Visions Unite, you consent to receive email notifications of 1) requests to help determine which messages are most important in groups you join and 2) messages people in your groups deem most important!</p>

        <span :if={@show_password == false} phx-click="show_password">👁</span>
        <span :if={@show_password == true} phx-click="hide_password">U</span>

        <.input field={@form[:password]} type={if @show_password == true do "text" else "password" end} label="Password" required value={@tmp_password} />
        <.input
          field={@form[:vetter]}
          type="checkbox"
          label="Participate in protecting people from spam and hate speech.  Please check this box if you are willing to review new messages and groups that may contain hateful ideas, spam or scams, or other content that should not be seen by anyone at all."
        />
        <details class="breathe">
          <summary>More information about participating in protecting people from spam and hate speach</summary>
          <p>The Visions Unite community relies on people willing to review new messages and groups that may contain hateful ideas, spam or scams, or other content that should not be seen by anyone at all.</p>
          <p>In this step:</p>
          <ul>
            <li>A few people from across the Visions Unite platform are selected at random to review each new message or group— this is not a per-group decision. Whether a message or group is acceptable for human consumption is a matter of universal values.</li>
            <li>Allowing this message does not mean you think it should be promoted to everyone in a group.</li>
            <li>Rejecting this message means that you think it should not be allowed on this platform at all.</li>
            <li>Please do not abuse your power in attempt to censor valid messages that you disagree with.</li>
          </ul>
          <p>(Specifically, [VETTING REACHED * 2] people are asked to review, and if [VETTING REACHED] people say it is OK, it becomes public; if [VETTING REACHED] people say it is not OK, the message is not shown to anyone else and the account that posted it is blocked.) TODO determine if it really works that way, probably move all this to a separate page.</p>
        </details>

        <:actions>
          <.button phx-disable-with="Creating account..." class="w-full">Create an account</.button>
        </:actions>
      </.simple_form>
    </main>
    """
  end

  def mount(_params, _session, socket) do
    changeset = Accounts.change_user_registration(%User{})

    socket =
      socket
      |> assign(trigger_submit: false, check_errors: false)
      |> assign_form(changeset)
      |> assign(show_password: false)
      |> assign(tmp_password: "")
      |> assign(:user_has_pending_groups, false)

    {:ok, socket, temporary_assigns: [form: nil]}
  end

  def handle_event("save", %{"user" => user_params}, socket) do
    case Accounts.register_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_confirmation_instructions(
            user,
            &url(~p"/users/confirm/#{&1}")
          )

        changeset = Accounts.change_user_registration(user)
        {:noreply, socket |> assign(trigger_submit: true) |> assign_form(changeset)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, socket |> assign(check_errors: true) |> assign_form(changeset)}
    end
  end

  def handle_event("validate", %{"user" => user_params}, socket) do
    socket =
      socket
      |> assign(tmp_password: user_params["password"])

    changeset = Accounts.change_user_registration(%User{}, user_params)

    {:noreply, assign_form(socket, Map.put(changeset, :action, :validate))}
  end

  def handle_event("show_password", _params, socket) do
    socket =
      socket
      |> assign(show_password: true)

    {:noreply, socket}
  end

  def handle_event("hide_password", _params, socket) do
    socket =
      socket
      |> assign(show_password: false)

    {:noreply, socket}
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    form = to_form(changeset, as: "user")

    if changeset.valid? do
      assign(socket, form: form, check_errors: false)
    else
      assign(socket, form: form)
    end
  end
end
