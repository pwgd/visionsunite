defmodule VisionsUniteWeb.UserSettingsLive do
  use VisionsUniteWeb, :live_view

  alias VisionsUnite.Accounts
  alias VisionsUnite.Groups

  def render(assigns) do
    ~H"""
    <div class="container space-y-12 divide-y">
      <.header class="text-center">
        Account Settings
        <:subtitle>Manage your account email address and password settings</:subtitle>
      </.header>

      <div>
        <.simple_form
          for={@email_form}
          id="email_form"
          phx-submit="update_email"
          phx-change="validate_email"
        >
          <.input field={@email_form[:email]} type="email" label="Email" required />

          <span :if={@show_password.email_pw == false} phx-click="toggle_password_visibility" phx-value-field-id={:email_pw}>👁</span>
          <span :if={@show_password.email_pw == true} phx-click="toggle_password_visibility" phx-value-field-id={:email_pw}>U</span>
          <.input
            field={@email_form[:current_password]}
            name="current_password"
            id="current_password_for_email"
            type={if @show_password.email_pw == true do "text" else "password" end}
            label="Current password"
            value={@email_form_current_password}
            required
          />
          <:actions>
            <.button phx-disable-with="Changing...">Change Email</.button>
          </:actions>
        </.simple_form>
      </div>
      <div>
        <.simple_form
          for={@password_form}
          id="password_form"
          phx-change="validate_password"
          phx-submit="update_password"
        >
          <.input
            field={@password_form[:email]}
            type="hidden"
            id="hidden_user_email"
            value={@current_email}
          />
          <span :if={@show_password.change_pw_new == false} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_new}>👁</span>
          <span :if={@show_password.change_pw_new == true} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_new}>U</span>
          <.input
            field={@password_form[:password]}
            type={if @show_password.change_pw_new == true do "text" else "password" end}
            label="New password"
            required
          />
          <span :if={@show_password.change_pw_confirm == false} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_confirm}>👁</span>
          <span :if={@show_password.change_pw_confirm == true} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_confirm}>U</span>
          <.input
            field={@password_form[:password_confirmation]}
            type={if @show_password.change_pw_confirm == true do "text" else "password" end}
            label="Confirm new password"
          />
          <span :if={@show_password.change_pw_old == false} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_old}>👁</span>
          <span :if={@show_password.change_pw_old == true} phx-click="toggle_password_visibility" phx-value-field-id={:change_pw_old}>U</span>
          <.input
            field={@password_form[:current_password]}
            name="current_password"
            type={if @show_password.change_pw_old == true do "text" else "password" end}
            label="Current password"
            id="current_password_for_password"
            value={@current_password}
            required
          />
          <:actions>
            <.button phx-disable-with="Changing...">Change Password</.button>
          </:actions>
        </.simple_form>
      </div>
      <div>
        <.simple_form
          for={@phone_number_form}
          id="phone_number_form"
          phx-change="validate_phone_number"
          phx-submit="update_phone_number"
        >
          <.input
            field={@phone_number_form[:phone_number]}
            type="tel"
            label="Phone Number"
            value={@current_phone_number}
          />
          <.input
            field={@phone_number_form[:receive_texts]}
            type="checkbox"
            label="Receive Texts? SMS message rates may apply."
          />
          <:actions>
            <.button phx-disable-with="Changing...">Change phone</.button>
          </:actions>
        </.simple_form>
      </div>
      <div>
        <.simple_form
          for={@vetter_form}
          id="vetter_form"
          phx-submit="update_vetter"
        >
          <.input
            field={@vetter_form[:vetter]}
            type="checkbox"
            label="Do you consent to be a vetter?"
          />
          <:actions>
            <.button phx-disable-with="Changing...">Change vetter status</.button>
          </:actions>
        </.simple_form>
      </div>
    </div>
    """
  end

  def mount(%{"token" => token}, _session, socket) do
    socket =
      case Accounts.update_user_email(socket.assigns.current_user, token) do
        :ok ->
          put_flash(socket, :info, "Email changed successfully.")

        :error ->
          put_flash(socket, :error, "Email change link is invalid or it has expired.")
      end

    {:ok, push_navigate(socket, to: ~p"/users/settings")}
  end

  def mount(_params, _session, socket) do
    user = socket.assigns.current_user
    email_changeset = Accounts.change_user_email(user)
    password_changeset = Accounts.change_user_password(user)
    phone_changeset = Accounts.change_user_phone(user)
    vetter_changeset = Accounts.change_user_vetter(user)
    user_has_pending_groups = Groups.user_has_pending_groups(socket.assigns.current_user)

    socket =
      socket
      |> assign(:current_password, nil)
      |> assign(:email_form_current_password, nil)
      |> assign(:current_email, user.email)
      |> assign(:current_phone_number, user.phone_number)
      |> assign(:email_form, to_form(email_changeset))
      |> assign(:password_form, to_form(password_changeset))
      |> assign(:show_password, %{
        email_pw: false,
        change_pw_new: false,
        change_pw_confirm: false,
        change_pw_old: false
      })
      |> assign(:phone_number_form, to_form(phone_changeset))
      |> assign(:user_has_pending_groups, user_has_pending_groups)
      |> assign(:vetter_form, to_form(vetter_changeset))
      |> assign(:trigger_submit, false)

    {:ok, socket}
  end

  def handle_event("validate_email", params, socket) do
    %{"current_password" => password, "user" => user_params} = params

    email_form =
      socket.assigns.current_user
      |> Accounts.change_user_email(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, email_form: email_form, email_form_current_password: password)}
  end

  def handle_event("update_email", params, socket) do
    %{"current_password" => password, "user" => user_params} = params
    user = socket.assigns.current_user

    case Accounts.apply_user_email(user, password, user_params) do
      {:ok, applied_user} ->
        Accounts.deliver_user_update_email_instructions(
          applied_user,
          user.email,
          &url(~p"/users/settings/confirm_email/#{&1}")
        )

        info = "A link to confirm your email change has been sent to the new address."
        {:noreply, socket |> put_flash(:info, info) |> assign(email_form_current_password: nil)}

      {:error, changeset} ->
        {:noreply, assign(socket, :email_form, to_form(Map.put(changeset, :action, :insert)))}
    end
  end

  def handle_event("validate_phone_number", params, socket) do
    %{"user" => user_params} = params

    phone_number_form =
      socket.assigns.current_user
      |> Accounts.change_user_phone(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, phone_number_form: phone_number_form)}
  end

  def handle_event("update_phone_number", params, socket) do
    %{"user" => user_params} = params
    user = socket.assigns.current_user

    case Accounts.update_user_phone(user, user_params) do
      {:ok} ->
        {:noreply, socket |> put_flash(:info, "Successfully updated phone number")}

      {:error, changeset} ->
        {:noreply, assign(socket, :phone_number_form, to_form(Map.put(changeset, :action, :insert)))}
    end
  end

  def handle_event("validate_password", params, socket) do
    %{"current_password" => password, "user" => user_params} = params

    password_form =
      socket.assigns.current_user
      |> Accounts.change_user_password(user_params)
      |> Map.put(:action, :validate)
      |> to_form()

    {:noreply, assign(socket, password_form: password_form, current_password: password)}
  end

  def handle_event("update_password", params, socket) do
    %{"current_password" => password, "user" => user_params} = params
    user = socket.assigns.current_user

    case Accounts.update_user_password(user, password, user_params) do
      {:ok, user} ->
        password_form =
          user
          |> Accounts.change_user_password(user_params)
          |> to_form()

        socket =
          socket
          |> assign(:trigger_submit, true)
          |> assign(:password_form, password_form)
          |> put_flash(:info, "Password updated.")

        {:noreply, socket}

      {:error, changeset} ->
        {:noreply, assign(socket, password_form: to_form(changeset))}
    end
  end

  def handle_event("update_vetter", params, socket) do
    %{"user" => user_params} = params
    user = socket.assigns.current_user

    {:ok, user} = Accounts.update_user_vetter_status(user, user_params)

    vetter_form =
      user
      |> Accounts.change_user_vetter(user_params)
      |> to_form()

    socket = socket
    |> put_flash(
      :info,
      "Thank you for updating your vetter status"
    )
    |> assign(:vetter_form, vetter_form)

    {:noreply, socket}
  end


  def handle_event("toggle_password_visibility", %{"field-id" => field_id}, socket) do
    field_id = String.to_atom(field_id)
    new_map = Map.put(socket.assigns.show_password, field_id, !socket.assigns.show_password[field_id])

    socket =
      socket
      |> assign(show_password: new_map)

    {:noreply, socket}
  end
end
