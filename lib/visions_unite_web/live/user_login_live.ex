defmodule VisionsUniteWeb.UserLoginLive do
  use VisionsUniteWeb, :live_view

  def render(assigns) do
    ~H"""
    <main class="container">
      <.header class="text-center">
        Sign in to account
        <:subtitle>
          Don't have an account?
          <.link navigate={~p"/users/register"} class="font-semibold text-brand hover:underline">
            Sign up
          </.link>
          for an account now.
        </:subtitle>
      </.header>

      <.simple_form for={@form} id="login_form" action={~p"/users/log_in"} phx-change="validate">
        <.input field={@form[:email]} type="email" label="Email" required value={@tmp_email} />

        <span :if={@show_password == false} phx-click="show_password">👁</span>
        <span :if={@show_password == true} phx-click="hide_password">U</span>

        <.input field={@form[:password]} type={if @show_password == true do "text" else "password" end} label="Password" required value={@tmp_password} />

        <:actions>
          <.input field={@form[:remember_me]} type="checkbox" label="Keep me logged in" value={@tmp_remember_me} />
          <.link href={~p"/users/reset_password"} class="text-sm font-semibold">
            Forgot your password?
          </.link>
        </:actions>
        <:actions>
          <.button phx-disable-with="Signing in..." class="w-full">
            Sign in <span aria-hidden="true">→</span>
          </.button>
        </:actions>
      </.simple_form>
    </main>
    """
  end

  def mount(_params, _session, socket) do
    email = live_flash(socket.assigns.flash, :email)
    form = to_form(%{"email" => email}, as: "user")
    socket =
      socket
      |> assign(form: form)
      |> assign(show_password: false)
      |> assign(tmp_email: "")
      |> assign(tmp_password: "")
      |> assign(tmp_remember_me: false)
      |> assign(:user_has_pending_groups, false)

    {:ok, socket, temporary_assigns: [form: form]}
  end

  def handle_event("validate", %{"user" => user_params}, socket) do
    socket =
      socket
      |> assign(tmp_email: user_params["email"])
      |> assign(tmp_password: user_params["password"])
      |> assign(tmp_remember_me: user_params["remember_me"])

    {:noreply, socket}
  end

  def handle_event("show_password", _params, socket) do
    socket =
      socket
      |> assign(show_password: true)

    {:noreply, socket}
  end

  def handle_event("hide_password", _params, socket) do
    socket =
      socket
      |> assign(show_password: false)

    {:noreply, socket}
  end
end
