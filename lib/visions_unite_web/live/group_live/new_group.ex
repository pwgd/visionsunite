defmodule VisionsUniteWeb.GroupLive.NewGroup do
  use VisionsUniteWeb, :live_view

  alias VisionsUnite.Groups
  alias VisionsUnite.GroupDrafts

  @impl true
  def render(assigns) do
    ~H"""
    <div class="container">
      <.header>
        New Group
      </.header>

      <.simple_form
        for={@form}
        id="group-form"
        phx-change="check_existing"
        phx-submit="save"
      >
        <.input field={@form[:name_text]} type="text" label="Name" value={@name} />

        <div :if={!Enum.empty?(@matched_groups)}>
          <p>Here are some groups with similar names. Perhaps consider joining one of these groups instead of creating a new one.</p>
          <ul >
            <li class="matched"  phx-value-name={matched_group.name} :for={matched_group <- @matched_groups} >
              <a href={~p"/show/#{matched_group.id}"}><%= matched_group.name %></a>
            </li>
          </ul>
        </div>

        <br/>

        <.input field={@form[:description_text]} type="textarea" label="Description" value={@description} />

        <.input field={@form[:author_id]} type="hidden" value={@current_user.id} />

        <:actions>
          <.button phx-disable-with="Saving...">Save</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    groups = Groups.list_vetted_groups()
    draft = GroupDrafts.get_draft_for_user(socket.assigns.current_user.id)
    user_has_pending_groups = Groups.user_has_pending_groups(socket.assigns.current_user)


    name = if !is_nil(draft) and !is_nil(draft.name_text) do
      draft.name_text
    else
      ""
    end


    matched_groups = if !is_nil(draft) and name !== "" do
      groups
      |> Enum.filter(fn group ->
        String.contains?(String.downcase(group.name), String.downcase(name))
      end)
    else
      []
    end

    socket =
      socket
      |> assign(:draft, draft)
      |> assign(:groups, groups)
      |> assign(:name, if !is_nil(draft) do draft.name_text else "" end)
      |> assign(:description, if !is_nil(draft) do draft.description_text else "" end)
      |> assign(:matched_groups, matched_groups)
      |> assign(:user_has_pending_groups, user_has_pending_groups)
      |> assign(form: to_form(%{}, as: "group"))

    {:ok, socket}
  end

  @impl true
  def handle_event("insert_name", %{"name" => name}, socket) do
    socket =
      socket
      |> assign(:name, name)
      |> assign(:matched_groups, [])

    {:noreply, socket}
  end

  @impl true
  def handle_event("check_existing", %{"group" => group_params}, socket) do
    name = group_params["name_text"]
    # NOTE we need to save description because of form resetting
    description = group_params["description"]
    same = socket.assigns.name

    matched_groups =
      cond do
        name == same ->
          []
        name == "" ->
          []
        true ->
          socket.assigns.groups
          |> Enum.filter(fn group ->
            String.contains?(String.downcase(group.name), String.downcase(name))
          end)
      end

    # Filter groups for string contains name

    socket =
      socket
      |> save_draft(group_params)
      |> assign(:matched_groups, matched_groups)
      # NOTE we need to save description because of form resetting
      |> assign(:description, description)
      |> assign(:name, name)

    {:noreply, socket}
  end

  def handle_event("save", %{"group" => group_params}, socket) do

    name_text = group_params["name_text"]
    description_text = group_params["description_text"]
    # We do indeed call this author ID for the draft group.
    creator_id = group_params["author_id"]

    group = %{
      "name" => name_text,
      "description" => description_text,
      "creator_id" => creator_id,
    }

    case Groups.create_group_and_request_vetting(group) do
      {:ok, _group} ->
        {:noreply,
         socket
         |> put_flash(:info, "Group created successfully.  Once Visions Unite members (like you!) have reviewed the new group request it will be visible to visitors and members.")
         |> redirect(to: ~p"/all_groups")}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, socket
        |> put_flash(:error, changeset.errors
          |> Enum.find(fn {key, _value} ->
            key == :name
          end)
          |> elem(1)
          |> elem(0)
        )}
    end
  end

  def save_draft(socket, group_params) do
    # check that socket.assigns.draft is not nil
    IO.puts("saving draft==================")

    socket =
      if is_nil(socket.assigns.draft) do
        # if it is, save a new group
        {:ok, draft} =
          GroupDrafts.create_draft(group_params)

        socket
        |> assign(:draft, draft)
      else
        # if it is not, we update that draft
        GroupDrafts.update_draft(socket.assigns.draft, group_params)

        socket
      end

      socket
  end

  # defp assign_form(socket, %Ecto.Changeset{} = changeset) do
  #  assign(socket, :form, to_form(changeset))
  # end
end
