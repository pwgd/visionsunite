defmodule VisionsUniteWeb.UserConfirmationController do
  use VisionsUniteWeb, :controller
  use Phoenix.Component
  use Phoenix.HTML

  alias VisionsUnite.Accounts

  def new(conn, _params) do
    render(conn, "new.html", user_has_pending_groups: false)
  end

  def create(conn, %{"email" => email}) do
    if user = Accounts.get_user_by_email(email) do
      Accounts.deliver_user_confirmation_instructions(
        user,
        &Routes.user_confirmation_url(conn, :edit, &1)
      )
    end

    conn
    |> put_flash(
      :info,
      "If your email is in our system and it has not been confirmed yet, " <>
        "you will receive an email with instructions shortly."
    )
    |> redirect(to: "/")
  end

  def edit(conn, %{"token" => token}) do
    render(conn, "edit.html", token: token, user_has_pending_groups: false)
  end

  # Do not log in the user after confirmation to avoid a
  # leaked token giving the user access to the account.
  def update(conn, %{"token" => token}) do
    case Accounts.confirm_user(token) do
      {:ok, _} ->
        conn
        |> put_flash(:info, "E-mail address confirmed successfully.  (Note that whether you are logged in or not did not change.)")
        |> redirect(to: "/")

      :error ->
        # If there is a current user and the account was already confirmed,
        # then odds are that the confirmation link was already visited, either
        # by some automation or by the user themselves, so we redirect without
        # a warning message.
        case conn.assigns do
          %{current_user: %{confirmed_at: confirmed_at}} when not is_nil(confirmed_at) ->
            redirect(conn, to: "/")

          %{} ->
            conn
            |> put_flash(:error, "E-mail address confirmation link is invalid or it has expired.")
            |> redirect(to: "/")
        end
    end
  end

  def email_field_markup(f, assigns) do
    if !assigns.current_user do

      assigns = Phoenix.Component.assign(assigns, :f, f)
      ~H"""
      <%= label @f, :email %>
      <input name="email" required type="email" />
      """
    else
      ~H"""
      <input name="email" required value={assigns.current_user.email} type="hidden" />
      """
    end
  end

  @doc """
  Renders account links depending on authentication status.
  """
  def account_links(assigns) do
    if !assigns.current_user do
      ~H"""
      <p>
        <%= link "Register", to: ~p"/users/register" %>
        <%= link "Log in", to: ~p"/users/log_in" %>
      </p>
      """
    else
      ""
    end
  end
end
