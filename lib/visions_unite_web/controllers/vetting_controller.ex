defmodule VisionsUniteWeb.VettingController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Accounts
  alias VisionsUnite.Groups
  alias VisionsUnite.Messages
  alias VisionsUnite.VettingRequests
  alias VisionsUnite.Vettings
  alias VisionsUnite.Vettings.Vetting

  def new_group_vetting(conn, %{"group_id" => group_id}) do
    with group <- Groups.get_group!(group_id),
         # NOTE vetting request might be stale (nil)
         vetting_request <- VettingRequests.get_vetting_request_for_group_and_user(
           group_id,
           conn.assigns.current_user.id
         ),
         {:vetting_request_not_nil, true} <- {:vetting_request_not_nil, !is_nil(vetting_request)}
    do

    changeset =
      Vettings.change_vetting(%Vetting{
        group_id: vetting_request.group_id
      })

    render(
      conn,
      "new_group_vetting.html",
      changeset: changeset,
      group: group,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
    else
      {:vetting_request_not_nil, false} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def new_message_vetting(conn, %{"message_id" => message_id}) do
    with message <- Messages.get_message!(message_id),
         htmlified <- VisionsUniteWeb.Utilities.htmlify(message),
         preloaded <- Messages.do_preload(htmlified, [:group, :topic]),
         vetting_request <- VettingRequests.get_vetting_request_for_message_and_user(
           message_id,
           conn.assigns.current_user.id
         ),
         {:vetting_request_not_nil, true} <- {:vetting_request_not_nil, !is_nil(vetting_request)}
    do

      group = Groups.get_group!(message.group_id)

      changeset =
        Vettings.change_vetting(%Vetting{
          message_id: vetting_request.message_id
        })

      render(
        conn,
        "new_message_vetting.html",
        changeset: changeset,
        group: group,
        message: preloaded,
        user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
      )

    else
      {:vetting_request_not_nil, false} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def allow(conn, %{"vetting" => vetting_params}) do
    vetting_params =
      Map.merge(vetting_params, %{
        "user_id" => conn.assigns.current_user.id,
        "vet" => true
      })

    case Vettings.create_vetting(vetting_params) do
      {:ok, _vetting} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or vetting requests list

        # Decrement the decline counter (possibly build up good
        #   will credit to half the unresponsive threshold)
        if conn.assigns.current_user.declined_to_participate > String.to_integer(System.get_env("UNRESPONSIVE_THRESHOLD", "50")) / -2 do
          Accounts.update_user_decline_counter(conn.assigns.current_user, :decrement)
        end

        conn
        |> put_flash(:info, "Success.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, :no_existing_vetting_request} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, %Ecto.Changeset{} = changeset} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or vetting requests list
        IO.puts "Error creating vetting: #{inspect changeset}"

        conn
        |> put_flash(:error, "Sorry, something went wrong.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def reject(conn, %{"vetting" => vetting_params}) do
    vetting_params =
      Map.merge(vetting_params, %{
        "user_id" => conn.assigns.current_user.id,
        "vet" => false
      })

    case Vettings.create_vetting(vetting_params) do
      {:ok, _vetting} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or vetting requests list
        conn
        |> put_flash(:info, "Success.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, :no_existing_vetting_request} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, %Ecto.Changeset{} = changeset} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or vetting requests list
        IO.puts "Error creating vetting: #{inspect changeset}"

        conn
        |> put_flash(:error, "Sorry, something went wrong.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end
end
