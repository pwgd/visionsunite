defmodule VisionsUniteWeb.PageController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Groups
  alias VisionsUnite.Sortition
  alias VisionsUnite.Supports
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.Messages
  alias VisionsUnite.NewNotifications

  def submit_vote(conn, %{"viewed" => _}) do
    NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id)
    |> Enum.each(&NewNotifications.delete_new_notification(&1))

    support_requests =
      SupportRequests.list_support_requests_for_user(conn.assigns.current_user.id)
      |> Enum.map(&Messages.get_message!(&1.message_id))

    if Enum.count(support_requests) !== 0 do
      render(conn, "vote.html",
        support_requests: support_requests,
        new_messages: []
      )
    else
      redirect(conn, to: "/")
    end
  end

  def submit_vote(conn, %{"support" => support_params}) do
    actioned =
      case support_params["support"] do
        nil ->
          {:error, "no vote"}

        "-1" ->
          "objected to"

        "1" ->
          "supported"
      end

    Supports.create_support(%{
      support: support_params["support"],
      note: support_params["note"],
      user_id: conn.assigns.current_user.id,
      message_id: support_params["message_id"]
    })

    support_requests =
      SupportRequests.list_support_requests_for_user(conn.assigns.current_user.id)
      |> Enum.map(&Messages.get_message!(&1.message_id))

    new_messages =
      NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id)
      |> Enum.map(fn notification ->
        Messages.get_message!(notification.message_id)
      end)

    case actioned do
      {:error, "no vote"} ->
        conn =
          conn
          |> put_flash(:error, "Please select \"support\" or \"reject\" below.")

        render(conn, "vote.html",
          support_requests: support_requests,
          new_messages: new_messages
        )

      _ ->
        conn =
          conn
          |> put_flash(:info, "Successfully #{actioned} message. Thank you!")

        if Enum.count(support_requests) !== 0 do
          render(conn, "vote.html",
            support_requests: support_requests,
            new_messages: new_messages
          )
        else
          redirect(conn, to: "/")
        end
    end
  end

  def voting(conn, _params) do
    support_requests =
      SupportRequests.list_support_requests_for_user(conn.assigns.current_user.id)
      |> Enum.map(&Messages.get_message!(&1.message_id))

    new_messages = NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id)

    render(conn, "vote.html",
      support_requests: support_requests,
      new_messages: new_messages
    )
  end

  def about(conn, _params) do
    render(conn, "about.html",
      active: :about,
      group_size: 0,
      changeset: :group_size,
      sortition_size: nil,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def update_about(conn, %{"group_size" => %{"group_size" => group_size}}) do
    {group_size, ""} = Integer.parse(group_size)
    sortition_size = Sortition.calculate_sortition_size(group_size)

    render(conn, "about.html",
      active: :about,
      group_size: group_size,
      changeset: :group_size,
      sortition_size: sortition_size,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end
end
