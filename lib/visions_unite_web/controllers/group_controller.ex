defmodule VisionsUniteWeb.GroupController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Groups
  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Messages
  alias VisionsUnite.NewNotifications
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.VettingRequests

  def all_groups(conn, _params) do
    if conn.assigns.current_user do
      cond do
        conn.assigns.current_user.declined_to_participate >= String.to_integer(System.get_env("UNRESPONSIVE_THRESHOLD", "50")) ->
          conn
          |> redirect(to: Routes.new_notification_path(conn, :unresponsive))

        VettingRequests.list_vetting_sought_for_user(conn.assigns.current_user.id) !== [] ->
          conn
          |> redirect(to: Routes.vetting_request_path(conn, :requesting_vetting))

        SupportRequests.list_support_requests_for_user(conn.assigns.current_user.id) !== [] ->
          conn
          |> redirect(to: Routes.support_request_path(conn, :requesting_support))

        NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id) !== [] ->
          conn
          |> redirect(to: Routes.new_notification_path(conn, :new_notifications))

        true ->
          groups = Groups.list_vetted_groups(conn.assigns.current_user.id)

          render(
            conn,
            "all_groups.html",
            groups: groups,
            page_title: "All Groups",
            active: :all_groups,
            current_user: conn.assigns.current_user,
            user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
          )
      end
    else
      groups = Groups.list_vetted_groups()

      render(
        conn,
        "all_groups.html",
        groups: groups,
        page_title: "All Groups",
        active: :all_groups,
        current_user: conn.assigns.current_user,
        user_has_pending_groups: false
      )
    end
  end

  def joined_groups(conn, _params) do
    groups = Groups.list_groups_for_user(conn.assigns.current_user.id)

    render(
      conn,
      "joined_groups.html",
      groups: groups,
      page_title: "Joined Groups",
      active: :joined_groups,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def show_my_unvetted_groups(conn, _params) do
    groups = Groups.list_pending_groups_for_user(conn.assigns.current_user.id)

    render(
      conn,
      "show_my_unvetted_groups.html",
      groups: groups,
      page_title: "My Proposed Groups",
      active: :show_my_unvetted_groups,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: true
    )
  end

  def new(conn, _params) do
    changeset = Groups.change_group(%Group{})
    render(conn, "new.html", changeset: changeset, page_title: "Create new group")
  end

  def create(conn, %{"group" => group_params}) do
    creator = conn.assigns.current_user

    case Groups.create_group_and_request_vetting(
           Map.merge(
             group_params,
             %{
               "creator_id" => creator.id
             }
           )
         ) do
      {:ok, _group} ->
        conn
        |> put_flash(
          :info,
          "Thank you for submitting a group for consideration. If it passes vetting, it will be available on the home page."
        )
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, page_title: "Create new group")
    end
  end

  def show_group(conn, %{"group_id" => group_id}) do
    group = Groups.get_group!(group_id)

    messages =
      Messages.list_messages_for_group(group.id, 10)
      |> VisionsUniteWeb.Utilities.htmlify()

    subscribed =
      case conn.assigns.current_user do
        nil ->
          false

        user ->
          subscription =
            Subscriptions.list_subscriptions_for_user(user.id)
            |> Enum.find(&(&1.group_id == group.id))

          if !is_nil(subscription) do
            true
          else
            false
          end
      end

    all_message_count = Messages.count_all_messages_for_group(group.id)
    message_count = Messages.count_messages_for_group(group.id)

    user_pending_message_count =
      if conn.assigns.current_user do
        Messages.count_pending_messages_for_user(group.id, conn.assigns.current_user.id)
      else
        0
      end
    url = Routes.group_url(conn, :show_group, group.id)
    qr = url |> EQRCode.encode() |> EQRCode.svg()

    render(
      conn,
      "show_group.html",
      group: group,
      has_any_messages: all_message_count > 0,
      has_supported_messages: message_count > 0,
      user_has_pending_messages: user_pending_message_count > 0,
      page_title: group.name,
      messages: messages,
      subscribed: subscribed,
      all_message_count: all_message_count,
      message_count: message_count,
      qr: qr,
      url: url,
      show_all: false,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def show_all_in_group(conn, %{"group_id" => group_id}) do
    group = Groups.get_group!(group_id)

    messages =
      Messages.list_all_messages_for_group(group.id, 10)
      |> VisionsUniteWeb.Utilities.htmlify()

    subscribed =
      if conn.assigns.current_user do
        Subscriptions.list_subscriptions_for_user(conn.assigns.current_user.id)
        |> Enum.find(&(&1.group_id == group.id))
      else
        false
      end

    all_message_count = Messages.count_all_messages_for_group(group.id)
    message_count = Messages.count_messages_for_group(group.id)
    unsupported_message_count = all_message_count - message_count
    user_pending_message_count =
      if conn.assigns.current_user do
        Messages.count_pending_messages_for_user(group.id, conn.assigns.current_user.id)
      else
        0
      end

    url = Routes.group_url(conn, :show_all_in_group, group.id)
    qr = url |> EQRCode.encode() |> EQRCode.svg()

    render(
      conn,
      "show_all.html",
      group: group,
      has_any_messages: all_message_count > 0,
      user_has_pending_messages: user_pending_message_count > 0,
      page_title: group.name,
      messages: messages,
      subscribed: subscribed,
      all_message_count: all_message_count,
      unsupported_message_count: unsupported_message_count,
      qr: qr,
      url: url,
      show_all: true,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def show_my_unvetted_in_group(conn, %{"group_id" => group_id}) do
    group = Groups.get_group!(group_id)

    all_message_count = Messages.count_all_messages_for_group(group.id)

    messages =
      Messages.list_unvetted_messages_for_author_in_group(conn.assigns.current_user.id, group_id)
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "show_my_unvetted.html",
      group: group,
      has_any_messages: all_message_count > 0,
      page_title: group.name,
      messages: messages,
      show_all: true,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end
end
