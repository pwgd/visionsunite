defmodule VisionsUniteWeb.SubscriptionController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Groups
  alias VisionsUnite.Subscriptions

  def subscribe(conn, %{"group_id" => group_id}) do
    case Subscriptions.create_subscription(%{
           user_id: conn.assigns.current_user.id,
           group_id: group_id
         }) do
      {:ok, subscription} ->
        group = Groups.get_group!(subscription.group_id)

        conn
        |> put_flash(:info, "Successfully subscribed to #{group.name}.")
        |> redirect(to: Routes.group_path(conn, :show_group, group.id))

      {:error, %Ecto.Changeset{} = _changeset} ->
        conn
        |> put_flash(:info, "Sorry, something went wrong.")
        |> redirect(to: Routes.group_path(conn, :index))
    end
  end

  def unsubscribe(conn, %{"group_id" => group_id}) do
    group = Groups.get_group!(group_id)

    subscription =
      Subscriptions.get_subscription_for_group_and_user(group_id, conn.assigns.current_user.id)

    {:ok, _subscription} = Subscriptions.delete_subscription(subscription)

    conn
    |> put_flash(:info, "Unsubscribed from #{group.name} successfully.")
    |> redirect(to: Routes.group_path(conn, :all_groups))
  end
end
