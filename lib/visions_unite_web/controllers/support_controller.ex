defmodule VisionsUniteWeb.SupportController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Accounts
  alias VisionsUnite.Groups
  alias VisionsUnite.Messages
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.Supports
  alias VisionsUnite.Supports.Support

  def new(conn, %{"message_id" => message_id}) do
    with message <- Messages.get_message!(message_id),
         htmlified <- VisionsUniteWeb.Utilities.htmlify(message),
         preloaded <- Messages.do_preload(htmlified, [:group, :topic]),
         support_request <- SupportRequests.get_support_request_for_message_and_user(
           message_id,
           conn.assigns.current_user.id
         ),
         {:support_request_not_nil, true} <- {:support_request_not_nil, !is_nil(support_request)}
    do

      group = Groups.get_group!(message.group_id)

      changeset =
        Supports.change_support(%Support{
          message_id: support_request.message_id
        })

      render(
        conn,
        "new.html",
        changeset: changeset,
        group: group,
        message: preloaded,
        user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
      )

    else
      {:support_request_not_nil, false} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def approve(conn, %{"support" => support_params}) do
    # TODO: Should we be merging here, or simply assigning?
    support_params =
      Map.merge(support_params, %{
        "user_id" => conn.assigns.current_user.id,
        "support" => true
      })

    case Supports.create_support(support_params) do
      {:ok, _support} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or support requests list

        # Decrement the decline counter (possibly build up good
        #   will credit to half the unresponsive threshold)
        if conn.assigns.current_user.declined_to_participate > String.to_integer(System.get_env("UNRESPONSIVE_THRESHOLD", "50")) / -2 do
          Accounts.update_user_decline_counter(conn.assigns.current_user, :decrement)
        end

        conn
        |> put_flash(:info, "Message approval recorded.  Thank you!")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, :no_existing_support_request} ->
        conn
        |> put_flash(:error, "Other people have already taken care of this request. Thank you so much. We will be needing your help soon enough, for sure.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, %Ecto.Changeset{} = _changeset} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or support requests list
        conn
        |> put_flash(:error, "Sorry, something went wrong.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def reject(conn, %{"support" => support_params}) do
    # TODO: Should we be merging here, or simply assigning?
    support_params =
      Map.merge(support_params, %{
        "user_id" => conn.assigns.current_user.id,
        "support" => false
      })

    case Supports.create_support(support_params) do
      {:ok, _support} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or support requests list
        conn
        |> put_flash(:info, "Message rejection recorded.  Thank you.")
        |> redirect(to: Routes.group_path(conn, :all_groups))

      {:error, %Ecto.Changeset{} = _changeset} ->
        # NOTE always redirecting to /all_groups so that group controller index can
        #   decide whether to redirect to new notifications or support requests list
        conn
        |> put_flash(:error, "Sorry, something went wrong.")
        |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end
end
