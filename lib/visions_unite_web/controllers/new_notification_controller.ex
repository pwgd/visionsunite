defmodule VisionsUniteWeb.NewNotificationController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Accounts
  alias VisionsUnite.NewNotifications
  alias VisionsUnite.Groups
  alias VisionsUnite.Messages

  def unresponsive(conn, _params) do
    render(conn, "unresponsive.html", user_has_pending_groups: false)
  end

  def new_notifications(conn, _params) do
    # TODO this actually doesn't make sense... just collect notifications, put the group
    #   or messsage in a tuple, just show all notifications..
    groups =
      NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id)
      |> Enum.filter(&(!is_nil(&1.group_id)))
      |> Enum.map(fn new_notification ->
        Groups.get_group!(new_notification.group_id)
      end)

    messages =
      NewNotifications.list_new_notifications_for_user(conn.assigns.current_user.id)
      |> Enum.filter(&(!is_nil(&1.message_id)))
      |> Enum.map(fn new_notification ->
        Messages.get_message!(new_notification.message_id)
      end)

    if Enum.count(groups) !== 0 || Enum.count(messages) !== 0 do
      render(
        conn,
        "new_notifications.html",
        groups: groups,
        messages: messages,
        user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
      )
    else
      conn
      |> redirect(to: Routes.group_path(conn, :all_groups))
    end
  end

  def mark_group_as_read(conn, %{"group_id" => group_id}) do
    notification =
      NewNotifications.get_new_notification_for_user_and_group(
        conn.assigns.current_user.id,
        group_id
      )

    NewNotifications.delete_new_notification(notification)

    conn
    |> redirect(to: Routes.new_notification_path(conn, :new_notifications))
  end

  def go_to_group_and_mark_read(conn, %{"group_id" => group_id}) do
    notification =
      NewNotifications.get_new_notification_for_user_and_group(
        conn.assigns.current_user.id,
        group_id
      )

    NewNotifications.delete_new_notification(notification)

    conn
    |> redirect(to: Routes.group_path(conn, :show_group, group_id))
  end

  def mark_message_as_read(conn, %{"message_id" => message_id}) do
    # TODO this actually doesn't make sense... just collect notifications, put the group
    #   or messsage in a tuple, just show all notifications..
    new_notification =
      NewNotifications.get_new_notification_for_user_and_message(
        conn.assigns.current_user.id,
        message_id
      )

    NewNotifications.delete_new_notification(new_notification)

    conn
    |> redirect(to: Routes.new_notification_path(conn, :new_notifications))
  end

  def go_to_topic_mark_message_read(conn, %{"topic_id" => topic_id, "message_id" => message_id}) do
    notification =
      NewNotifications.get_new_notification_for_user_and_message(
        conn.assigns.current_user.id,
        message_id
      )

    NewNotifications.delete_new_notification(notification)

    redirect(conn, to: ~p"/topic/#{topic_id}")
  end

  def mark_responsive(conn, _params) do
    Accounts.update_user_decline_counter(conn.assigns.current_user, :reset)

    conn
    |> redirect(to: Routes.group_path(conn, :all_groups))
  end
end
