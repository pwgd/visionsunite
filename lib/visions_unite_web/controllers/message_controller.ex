defmodule VisionsUniteWeb.MessageController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.Messages
  alias VisionsUnite.Messages.Message
  alias VisionsUnite.Groups
  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Topics

  def my_authored_messages(conn, _params) do
    messages =
      Messages.list_messages_for_author(conn.assigns.current_user.id)
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "my_authored_messages.html",
      messages: messages,
      active: :my_authored_messages,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def joined_messages(conn, _params) do
    messages =
      Messages.list_messages_for_user(conn.assigns.current_user.id)
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "joined_messages.html",
      messages: messages,
      active: :joined_messages,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def all_messages(conn, _params) do
    messages =
      Messages.list_all_messages()
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "all_messages.html",
      messages: messages,
      active: :all_messages,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def messages_in_topic(conn, %{"topic_id" => topic_id}) do
    topic = Topics.get_topic!(topic_id)

    messages =
      Messages.list_messages_for_topic(topic_id)
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "messages_in_topic.html",
      messages: messages,
      topic: topic,
      active: :all_messages,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def all_messages_in_topic(conn, %{"topic_id" => topic_id}) do
    topic = Topics.get_topic!(topic_id)

    messages =
      Messages.list_all_messages_for_topic(topic_id)
      |> VisionsUniteWeb.Utilities.htmlify()

    render(
      conn,
      "all_messages_in_topic.html",
      messages: messages,
      topic: topic,
      active: :all_messages,
      current_user: conn.assigns.current_user,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end

  def new(conn, %{"group_id" => group_id}) do
    group = Groups.get_group!(group_id)
    if !Group.is_active?(group) do
      conn
        |> put_status(:forbidden)
        |> put_view(VisionsUniteWeb.ErrorView)
        |> render(:"403_inner")
    else
      topic_options =
        Topics.list_topics_for_group(group_id)
        |> Enum.map(fn topic ->
          {topic.name, topic.id}
        end)

      changeset =
        Messages.change_message(%Message{
          group_id: group_id
        })

      render(
        conn,
        "new.html",
        changeset: changeset,
        group: group,
        topic_options: topic_options,
        user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
      )
    end
  end

  def create(conn, %{"message" => message_params}) do
    author = conn.assigns.current_user

    message_params =
      if !is_nil(message_params["new_topic"]) && message_params["new_topic"] !== "" do
        topic =
          case Topics.get_topic_by_group_and_name(
                 message_params["group_id"],
                 message_params["new_topic"]
               ) do
            nil ->
              {:ok, topic} =
                Topics.create_topic(%{
                  name: message_params["new_topic"],
                  group_id: message_params["group_id"]
                })

              topic

            existing_topic ->
              existing_topic
          end

        Map.merge(message_params, %{
          "topic_id" => topic.id
        })
      else
        message_params
      end

    case Messages.finalize_message_and_request_vetting(
           Map.merge(
             message_params,
             %{
               "author_id" => author.id
             }
           )
         ) do
      {:ok, _message} ->
        group = Groups.get_group!(message_params["group_id"])

        conn
        |> put_flash(:info, "Message created successfully.")
        |> redirect(to: Routes.group_path(conn, :show_group, group.id))

      {:error, %Ecto.Changeset{} = changeset} ->
        group = Groups.get_group!(message_params["group_id"])

        topic_options =
          Topics.list_topics_for_group(message_params["group_id"])
          |> Enum.map(fn topic ->
            {topic.name, topic.id}
          end)

        render(
          conn,
          "new.html",
          changeset: changeset,
          group: group,
          topic_options: topic_options,
          user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
        )
    end
  end
end
