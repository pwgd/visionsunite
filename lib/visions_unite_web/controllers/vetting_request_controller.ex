defmodule VisionsUniteWeb.VettingRequestController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.VettingRequests
  alias VisionsUnite.Messages
  alias VisionsUnite.Groups

  def requesting_vetting(conn, _params) do
    {groups, messages} =
      VettingRequests.list_vetting_sought_for_user(conn.assigns.current_user.id)
      |> Enum.reduce({[], []}, fn vetting_request, {groups, messages} ->
        if !is_nil(vetting_request.message_id) do
          message =
            Messages.get_message!(vetting_request.message_id)
            |> Messages.do_preload([:group])

          {groups, [message | messages]}
        else
          group = Groups.get_group!(vetting_request.group_id)
          {[group | groups], messages}
        end
      end)

    if (Enum.count(groups) == 0 and Enum.count(messages) == 0) do
      conn
      |> redirect(to: Routes.group_path(conn, :joined_groups))
    end

    render(conn, "requesting_vetting.html", groups: groups, messages: messages,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end
end
