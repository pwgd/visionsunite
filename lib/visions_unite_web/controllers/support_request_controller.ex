defmodule VisionsUniteWeb.SupportRequestController do
  use VisionsUniteWeb, :controller

  alias VisionsUnite.SupportRequests
  alias VisionsUnite.Messages
  alias VisionsUnite.Groups

  def requesting_support(conn, _params) do
    {groups, messages} =
      SupportRequests.list_support_requests_for_user(conn.assigns.current_user.id)
      |> Enum.reduce({[], []}, fn support_request, {groups, messages} ->
        if !is_nil(support_request.message_id) do
          message =
            Messages.get_message!(support_request.message_id)
            |> Messages.do_preload([:group])

          {groups, [message | messages]}
        else
          group = Groups.get_group!(support_request.group_id)
          {[group | groups], messages}
        end
      end)

    render(
      conn,
      "requesting_support.html",
      groups: groups,
      messages: messages,
      user_has_pending_groups: Groups.user_has_pending_groups(conn.assigns.current_user)
    )
  end
end
