defmodule VisionsUniteWeb.Utilities do
  def htmlify(message) when is_map(message) do
    markdown_text =
      case Earmark.as_html(message.text, pure_links: false, compact_output: true) do
        {:ok, html, []} ->
          html
          |> HtmlSanitizeEx.markdown_html()
          |> Linkify.link(new_window: true, rel: "noopener noreferrer")

        _error ->
          message.text
      end

    %{message | text: markdown_text}
  end

  def htmlify(messages) when is_list(messages) do
    messages
    |> Enum.map(fn message ->
      htmlify(message)
    end)
  end
end
