defmodule VisionsUnite.Messages.Message do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Topics.Topic

  schema "messages" do
    field :text, :string
    field :vetted, :boolean
    field :fully_supported, :boolean
    field :sortition_number, :integer

    belongs_to :author, User
    belongs_to :group, Group
    belongs_to :topic, Topic

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [
      :text,
      :vetted,
      :fully_supported,
      :sortition_number,
      :author_id,
      :group_id,
      :topic_id
    ])
    |> validate_required([:text, :author_id, :group_id, :topic_id])
    |> validate_group_is_active()
  end

  def validate_group_is_active(changeset) do
    group_id = get_field(changeset, :group_id)
    if Group.is_active?(group_id) == true do
      changeset
    else
      add_error(changeset, :group_id, "This group is not active.  Messages cannot be posted to inactive (not yet vetted etc) groups.")
    end
  end
end
