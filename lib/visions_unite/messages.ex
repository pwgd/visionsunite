defmodule VisionsUnite.Messages do
  @moduledoc """
  The Messages context.
  """

  import Ecto.Query, warn: false
  alias VisionsUnite.Repo

  alias VisionsUnite.Messages.Message
  alias VisionsUnite.MessageDrafts
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.VettingRequests

  @doc """
  Returns the list of all messages.

  ## Examples

      iex> list_absolutely_all_messages()
      [%Message{}, ...]

  """
  def list_absolutely_all_messages() do
    Repo.all(Message)
  end

  @doc """
  Returns the list of all vetted and approved messages.

  ## Examples

      iex> list_all_messages()
      [%Message{}, ...]

  """
  def list_all_messages() do
    query =
      from m in Message,
        where:
          m.vetted == true and
            m.fully_supported == true

    Repo.all(query)
  end

  @doc """
  Returns the list of messages for a given author.

  ## Examples

      iex> list_messages_for_author(user_id)
      [%Message{}, ...]

  """
  def list_messages_for_author(author_id) do
    query =
      from m in Message,
      where: m.author_id == ^author_id

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns unvetted messages by a user in a group.

  ## Examples

      iex> list_unvetted_messages_for_author_in_group(user_id, group_id)
      [%Message{}, ...]

  """
  def list_unvetted_messages_for_author_in_group(author_id, group_id) do
    query =
      from m in Message,
        where: m.author_id == ^author_id and
          is_nil(m.vetted) and
          m.group_id == ^group_id


    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns the list of vetted and approved messages for a given user (messages in groups this user has subscribed to).

  ## Examples

      iex> list_messages_for_user(298)
      [%Message{}, ...]

  """
  def list_messages_for_user(user_id) do
    subscription_ids =
      Subscriptions.list_subscriptions_for_user(user_id)
      |> Enum.map(& &1.group_id)

    query =
      from m in Message,
        where:
          m.group_id in ^subscription_ids and
            m.vetted == true and
            m.fully_supported == true

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns the list of vetted and approved messages for a given group.

  ## Examples

      iex> list_messages_for_group(43)
      [%Message{}, ...]

  """
  def list_messages_for_group(group_id) do
    query =
      from m in Message,
        where:
          m.group_id == ^group_id and
            m.vetted == true and
            m.fully_supported == true,
        order_by: [desc: :inserted_at]

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns the list of vetted and approved messages for a given group.
    ... accepts a limit

  ## Examples

      iex> list_messages_for_group(43, 10)
      [%Message{}, ...]

  """
  def list_messages_for_group(group_id, limit) do
    query =
      from m in Message,
        where:
          m.group_id == ^group_id and
            m.vetted == true and
            m.fully_supported == true,
        order_by: [desc: :inserted_at],
      limit: ^limit

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns list of all vetted messages for a given group.

  This is inclusive of unsupported as well as supported messages.

  ## Examples

      iex> list_all_messages_for_group(43)
      [%Message{}, ...]

  """
  def list_all_messages_for_group(group_id) do
    query =
      from m in Message,
        where:
          m.group_id == ^group_id and
            m.vetted == true,
        order_by: [desc: :inserted_at]

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns list of all vetted messages for a given group.

  This is inclusive of unsupported as well as supported messages.
    ... accepts a limit

  ## Examples

      iex> list_all_messages_for_group(43, 10)
      [%Message{}, ...]

  """
  def list_all_messages_for_group(group_id, limit) do
    query =
      from m in Message,
        where:
          m.group_id == ^group_id and
            m.vetted == true,
        order_by: [desc: :inserted_at],
      limit: ^limit

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns the list of vetted and approved messages for a given topic.

  ## Examples

      iex> list_messages_for_topic(43)
      [%Message{}, ...]

  """
  def list_messages_for_topic(topic_id) do
    query =
      from m in Message,
        where:
          m.topic_id == ^topic_id and
            m.vetted == true and
            m.fully_supported == true,
        order_by: [asc: :inserted_at]

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns the list of all vetted messages for a given topic.

  ## Examples

      iex> list_all_messages_for_topic(43)
      [%Message{}, ...]

  """
  def list_all_messages_for_topic(topic_id) do
    query =
      from m in Message,
        where:
          m.topic_id == ^topic_id and
            m.vetted == true,
        order_by: [asc: :inserted_at]

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns count of all vetted messages for a given group.

  This is inclusive of unsupported as well as supported messages.

  ## Examples

      iex> count_all_messages_for_group(43)
      147
  """
  def count_all_messages_for_group(group_id) do
    query =
      from m in Message,
        where: m.group_id == ^group_id and
          m.vetted == true

    Repo.aggregate(query, :count)
  end

  @doc """
  Returns count of supported and vetted messages for a given group.

  ## Examples

      iex> count_messages_for_group(43)
      98
  """
  def count_messages_for_group(group_id) do
    query =
      from m in Message,
        where:
          m.group_id == ^group_id
        and
          m.vetted == true
        and
          m.fully_supported == true

    Repo.aggregate(query, :count)
  end

  @doc """
  Returns the list of all messages for a given author.

  ## Examples

      iex> list_all_messages_for_user(43)
      [%Message{}, ...]

  """
  def list_all_messages_for_author(author_id) do
    query =
      from m in Message,
        where: m.author_id == ^author_id

    Repo.all(query)
    |> do_preload()
  end

  @doc """
  Returns count of all unvetted messages for a given group and user.

  ## Examples

      iex> count_pending_messages_for_user(43, 7)
      2
  """
  def count_pending_messages_for_user(group_id, user_id) do
    query =
      from m in Message,
        where: m.group_id == ^group_id and
          is_nil(m.vetted) and
          m.author_id == ^user_id

    Repo.aggregate(query, :count)
  end

  @doc """
  Gets a single message.

  Raises `Ecto.NoResultsError` if the Message does not exist.

  ## Examples

      iex> get_message!(123)
      %Message{}

      iex> get_message!(456)
      ** (Ecto.NoResultsError)

  """
  def get_message!(id) do
    Repo.get!(Message, id)
    |> do_preload()
    |> VisionsUniteWeb.Utilities.htmlify()
  end


  @doc """
  Creates a message.

  ## Examples

      iex> create_message(%{field: value})
      {:ok, %Message{}}

      iex> create_message(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_message(attrs \\ %{}) do
    %Message{}
    |> Message.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Finalizes a message and requests vetting.

  ## Examples

      iex> finalize_message_and_request_vetting(%{field: value})
      {:ok, %Message{}}

      iex> finalize_message_and_request_vetting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def finalize_message_and_request_vetting(message_data) do
    case create_message(message_data) do
      {:ok, message} ->
        # Let's now seek supporters for this message
        VettingRequests.seek_initial_message_vetters(message)

        draft = MessageDrafts.get_draft_for_user_and_group(message_data["author_id"], message_data["group_id"])
        if !is_nil(draft) do
          MessageDrafts.delete_draft(draft)
        end

        {:ok, message}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a message.

  ## Examples

      iex> update_message(message, %{field: new_value})
      {:ok, %Message{}}

      iex> update_message(message, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_message(%Message{} = message, attrs) do
    message
    |> Message.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a message.

  ## Examples

      iex> delete_message(message)
      {:ok, %Message{}}

      iex> delete_message(message)
      {:error, %Ecto.Changeset{}}

  """
  def delete_message(%Message{} = message) do
    Repo.delete(message)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking message changes.

  ## Examples

      iex> change_message(message)
      %Ecto.Changeset{data: %Message{}}

  """
  def change_message(%Message{} = message, attrs \\ %{}) do
    Message.changeset(message, attrs)
  end

  def do_preload(message) do
    Repo.preload(message, [:group, :topic])
  end
  def do_preload(message, preloads) do
    Repo.preload(message, preloads)
  end
end
