defmodule VisionsUnite.Subscriptions.Subscription do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Accounts.User

  schema "subscriptions" do
    belongs_to :group, Group
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(subscription, attrs) do
    subscription
    |> cast(attrs, [:group_id, :user_id])
    |> validate_required([:group_id, :user_id])
    |> unique_constraint(:unique_subscription, name: :unique_subscription)
  end
end
