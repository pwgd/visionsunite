defmodule VisionsUnite.Vettings do
  @moduledoc """
  The Vettings context.
  """

  import Ecto.Query, warn: false
  alias VisionsUnite.Repo

  alias VisionsUnite.Accounts
  alias VisionsUnite.Vettings.Vetting
  alias VisionsUnite.VettingRequests
  alias VisionsUnite.Groups
  alias VisionsUnite.Messages
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.NewNotifications
  alias VisionsUnite.Topics

  @doc """
  Returns the list of vettings.

  ## Examples

      iex> list_vettings()
      [%Vetting{}, ...]

  """
  def list_vettings do
    Repo.all(Vetting)
  end

  @doc """
  Returns a count of vettings for a particular group.

  ## Examples

      iex> count_vettings_for_group(%Message{})
      3

      iex> count_vettings_for_group(482)
      5

  """
  def count_vettings_for_group(nil), do: 0

  def count_vettings_for_group(group) when is_map(group) do
    count_vettings_for_group(group.id)
  end

  def count_vettings_for_group(group_id) do
    vetted_query =
      from s in Vetting,
        where:
          s.group_id == ^group_id and
            s.vet == true

    number_vetted = Repo.aggregate(vetted_query, :count)

    rejected_query =
      from s in Vetting,
      where:
        s.group_id == ^group_id and
          s.vet == false

    number_rejected = Repo.aggregate(rejected_query, :count)

    number_vetted - number_rejected
  end

  @doc """
  This function returns the list of vettings == true for a given group.

  ## Examples

      iex> list_vettings_for_group(group)
      [%Vetting{} ...]

  """
  def list_vettings_for_group(group) do
    query =
      from s in Vetting,
        where:
          s.group_id == ^group.id and
            s.vet == true

    Repo.all(query)
  end

  @doc """
  This function returns the list of all vettings (even vetting == false) for a given group.

  ## Examples

      iex> list_all_vettings_for_group(group)
      [%Vetting{} ...]

  """
  def list_all_vettings_for_group(group) do
    query =
      from s in Vetting,
        where: s.group_id == ^group.id

    Repo.all(query)
  end

  @doc """
  Returns a count of vettings for a particular message.

  ## Examples

      iex> count_vettings_for_message(%Message{})
      32

      iex> count_vettings_for_message(482)
      230

  """
  def count_vettings_for_message(nil), do: 0

  def count_vettings_for_message(message) when is_map(message) do
    count_vettings_for_message(message.id)
  end

  def count_vettings_for_message(message_id) do
    vetted_query =
      from s in Vetting,
        where:
          s.message_id == ^message_id and
            s.vet == true

    number_vetted = Repo.aggregate(vetted_query, :count)

    rejected_query =
      from s in Vetting,
      where:
        s.message_id == ^message_id and
          s.vet == false

    number_rejected = Repo.aggregate(rejected_query, :count)

    number_vetted - number_rejected
  end

  @doc """
  This function returns the list of vettings == true for a given message.

  ## Examples

      iex> list_vettings_for_message(message)
      [%Vetting{} ...]

  """
  def list_vettings_for_message(message) do
    query =
      from s in Vetting,
        where:
          s.message_id == ^message.id and
            s.vet == true

    Repo.all(query)
  end

  @doc """
  This function returns the list of all vettings (even vetting == false) for a given message.

  ## Examples

      iex> list_all_vettings_for_message(message)
      [%Vetting{} ...]

  """
  def list_all_vettings_for_message(message) do
    query =
      from s in Vetting,
        where: s.message_id == ^message.id

    Repo.all(query)
  end

  @doc """
  Gets a single vetting.

  Raises `Ecto.NoResultsError` if the Vetting does not exist.

  ## Examples

      iex> get_vetting!(123)
      %Vetting{}

      iex> get_vetting!(456)
      ** (Ecto.NoResultsError)

  """
  def get_vetting!(id), do: Repo.get!(Vetting, id)

  @doc """
  Creates a vetting record.  When a person has vetted (approved or rejected) a
  group or a message, that act is what we are calling "creating a vetting",
  which triggers figuring out if the full vetting process is over.

  ## Examples

      iex> create_vetting(%{field: value})
      {:ok, %Vetting{}}

      iex> create_vetting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_vetting(attrs \\ %{}) do
    group_id = attrs["group_id"]
    message_id = attrs["message_id"]
    user_id = attrs["user_id"]

    vetting =
      %Vetting{}
      |> Vetting.changeset(attrs)
      |> Repo.insert()

    # Remove the existing vetting request from the user that just clicked
    # NOTE! the vetting request might be stale...
    existing_vetting_request =
      if group_id do
        VettingRequests.get_vetting_request_for_group_and_user(group_id, user_id)
      else
        VettingRequests.get_vetting_request_for_message_and_user(message_id, user_id)
      end

    if !is_nil(existing_vetting_request) do
      VettingRequests.delete_vetting_request(existing_vetting_request)

      # Vetting Request has been deleted.... now we check for vetting
      vetting_count =
        if group_id do
          count_vettings_for_group(group_id)
        else
          count_vettings_for_message(message_id)
        end

      cond do
        vetting_count >= String.to_integer(System.get_env("VETTING_REACHED") || "5") ->
          # if group or message has been fully vetted, remove all vetting requests
          # for this group or message because the goal has already been reached
          if group_id do
            VettingRequests.delete_all_vetting_requests_for_group(group_id)

            # set the group as fully vetted
            group = Groups.get_group!(group_id)
            Groups.update_group(group, %{vetted: true})

            # also, create new group notifications for all VisionsUnite users
            Accounts.list_users()
            |> Enum.each(fn user ->
              NewNotifications.create_new_notification(%{
                group_id: group.id,
                user_id: user.id
              })
            end)
          else
            VettingRequests.delete_all_vetting_requests_for_message(message_id)

            # set the message as fully vetted
            message = Messages.get_message!(message_id)
            Messages.update_message(message, %{vetted: true})

            # set the topic as fully vetted
            topic = Topics.get_topic!(message.topic_id)
            Topics.update_topic(topic, %{vetted: true})

            # now, begin sortition phase for this message
            SupportRequests.seek_supporters(message)

            # also, create new message notifications for the subscribers of this group
            Subscriptions.list_subscriptions_for_group(group_id)
            |> Enum.each(fn subscription ->
              NewNotifications.create_new_notification(%{
                message_id: message.id,
                user_id: subscription.user_id
              })
            end)
          end
        vetting_count < String.to_integer(System.get_env("VETTING_DENIED_REACHED") || "-5") ->
          # if group or message has failed vetting, remove all vetting requests
          # for this group or message because enough vetters have rejected its presence
          if group_id do
            VettingRequests.delete_all_vetting_requests_for_group(group_id)

            # set the group as rejected
            group = Groups.get_group!(group_id)
            Groups.update_group(group, %{vetted: false})

            # Send message letting person know their group is rejected.
            Accounts.UserNotifier.deliver_group_rejected(group)
          else
            VettingRequests.delete_all_vetting_requests_for_message(message_id)

            # set the message as rejected
            message = Messages.get_message!(message_id)
            Messages.update_message(message, %{vetted: false})

            # Send message letting person know their message is rejected.
            Accounts.UserNotifier.deliver_message_rejected(message)

            # Topics are not being deleted, since it is possible that the same topic is being used for other messages in the vetting process
          end
        true -> nil
      end

      # return the vetting
      vetting
    else
      {:error, :no_existing_vetting_request}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking vetting changes.

  ## Examples

      iex> change_vetting(vetting)
      %Ecto.Changeset{data: %Vetting{}}

  """
  def change_vetting(%Vetting{} = vetting, attrs \\ %{}) do
    Vetting.changeset(vetting, attrs)
  end
end
