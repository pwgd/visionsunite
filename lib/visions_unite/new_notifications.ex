defmodule VisionsUnite.NewNotifications do
  @moduledoc """
  The NewNotifications context.
  """

  import Ecto.Query, warn: false

  alias VisionsUnite.Repo
  alias VisionsUnite.NewNotifications.NewNotification

  @doc """
  Returns the list of new_notifications.

  ## Examples

      iex> list_new_notifications()
      [%NewNotification{}, ...]

  """
  def list_new_notifications do
    Repo.all(NewNotification)
  end

  @doc """
  Returns the list of new_notifications for a particular user.

  ## Examples

      iex> list_new_notifications_for_user(user_id)
      [%NewNotification{}, ...]

  """
  def list_new_notifications_for_user(user_id) do
    query =
      from n in NewNotification,
        where: n.user_id == ^user_id

    Repo.all(query)
  end

  @doc """
  Returns the list of new_notifications for a particular message.

  ## Examples

      iex> list_new_notifications_for_message(message_id)
      [%NewNotification{}, ...]

  """
  def list_new_notifications_for_message(message_id) do
    query =
      from ep in NewNotification,
        where: ep.message_id == ^message_id

    Repo.all(query)
  end

  @doc """
  Gets a single new_notification.

  Raises `Ecto.NoResultsError` if the NewNotification does not exist.

  ## Examples

      iex> get_new_notification!(123)
      %NewNotification{}

      iex> get_new_notification!(456)
      ** (Ecto.NoResultsError)

  """
  def get_new_notification!(id), do: Repo.get!(NewNotification, id)

  @doc """
  Gets a single new_notification for a given user and group.

  ## Examples

      iex> get_new_notification_for_user_and_group(123, 238)
      %NewNotification{}
  """
  def get_new_notification_for_user_and_group(user_id, group_id) do
    query =
      from nn in NewNotification,
        where:
          nn.user_id == ^user_id and
            nn.group_id == ^group_id

    Repo.one(query)
  end

  @doc """
  Gets a single new_notification for a given user and message.

  ## Examples

      iex> get_new_notification_for_user_and_message(123, 238)
      %NewNotification{}
  """
  def get_new_notification_for_user_and_message(user_id, message_id) do
    query =
      from nn in NewNotification,
        where:
          nn.user_id == ^user_id and
            nn.message_id == ^message_id

    Repo.one(query)
  end

  @doc """
  Creates a new_notification.

  ## Examples

      iex> create_new_notification(%{field: value})
      {:ok, %NewNotification{}}

      iex> create_new_notification(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_new_notification(attrs \\ %{}) do
    %NewNotification{}
    |> NewNotification.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a new_notification.

  ## Examples

      iex> delete_new_notification(new_notification)
      {:ok, %NewNotification{}}

      iex> delete_new_notification(new_notification)
      {:error, %Ecto.Changeset{}}

  """
  def delete_new_notification(nil) do
    nil
  end

  def delete_new_notification(%NewNotification{} = new_notification) do
    Repo.delete(new_notification)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking new_notification changes.

  ## Examples

      iex> change_new_notification(new_notification)
      %Ecto.Changeset{data: %NewNotification{}}

  """
  def change_new_notification(%NewNotification{} = new_notification, attrs \\ %{}) do
    NewNotification.changeset(new_notification, attrs)
  end
end
