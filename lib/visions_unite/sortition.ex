defmodule VisionsUnite.Sortition do
  @moduledoc """
  The Sortition context.
  """

  alias VisionsUnite.Accounts
  alias Statistics.Distributions.Normal

  #
  # This function gets the sortition size for a given population number
  #
  # The sortition is also known as the sample size, which is calculated by a formula
  # from statistics.
  #

  def calculate_sortition_size(0), do: 0

  def calculate_sortition_size(group_count) do
    ## Calculating Sample Size with Finite Population from https://www.youtube.com/watch?v=gLD4tENS82c
    ##
    ##  c = Confidence Level = 95%
    ##  p = Population Proportion = 0.5 (most conservative)
    ##  e = Margin of Error aka Confidence Interval = 0.05 (5%)
    ##  pop = Population Size = 2500
    ##
    ##  a_div_2 = Alpha divided by 2 = (1-c)/2 = 0.025
    ##  z = Z-Score = norm.s.inv(1-a_div_2) = 1.9603949169253396
    ##  # note: this is the inverse of the CDF of the standard normal distribution.
    ##
    ##  numerator = (z^2) * (p*(1-p))/(e^2) = 384.3148230306708
    ##  denominator = 1 + (z^2) * (p*(1-p))/(e^2*pop) = 1.1537259292122684
    ##  Sample Size (rounded) = numerator/denominator = 333

    c = String.to_float(System.get_env("CONFIDENCE_LEVEL", "0.95"))
    p = String.to_float(System.get_env("POPULATION_PROPORTION", "0.5"))
    e = String.to_float(System.get_env("CONFIDENCE_INTERVAL", "0.05"))

    pop =
      if group_count == %{} do
        Accounts.count_users()
      else
        group_count
      end

    a_div_2 = (1 - c) / 2

    z = Normal.ppf().(1 - a_div_2)

    numerator = Float.pow(z, 2) * (p * (1 - p)) / Float.pow(e, 2)

    denominator = 1 + Float.pow(z, 2) * (p * (1 - p)) / (Float.pow(e, 2) * pop)

    Kernel.round(numerator / denominator)
  end
end
