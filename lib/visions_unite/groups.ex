defmodule VisionsUnite.Groups do
  @moduledoc """
  The Groups context.
  """

  import Ecto.Query, warn: false
  alias VisionsUnite.Repo

  alias VisionsUnite.Groups.Group
  alias VisionsUnite.GroupDrafts
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.VettingRequests

  @doc """
  Returns the list of all_groups.  This should not be used.  Use list_vetted_groups().

  ## Examples

      iex> list_all_groups()
      [%Group{}, ...]

  """
  def list_all_groups do
    Repo.all(Group)
    |> Enum.map(fn group->
      Map.merge(group, %{
        subscribed?: false
      })
    end)

  end

  @doc """
  Returns the list of subscribed groups for a particular user.

  ## Examples

      iex> list_groups_for_user(832)
      [%Group{}, ...]

  """
  def list_groups_for_user(user_id) do
    subscriptions =
      Subscriptions.list_subscriptions_for_user(user_id)
      |> Enum.map(& &1.group_id)

    query =
      from g in Group,
        where: g.id in ^subscriptions

    Repo.all(query)
    |> Enum.map(fn group->
      Map.merge(group, %{
        subscribed?: true
      })
    end)

  end

  @doc """
  Returns the list of unvetted groups for a particular user.

  ## Examples

      iex> list_pending_groups_for_user(832)
      [%Group{}, ...]

  """
  def list_pending_groups_for_user(user_id) do
    query =
      from g in Group,
        where: g.creator_id == ^user_id and
          is_nil(g.vetted)

    Repo.all(query)
    |> Enum.map(fn group->
      Map.merge(group, %{
        subscribed?: true
      })
    end)

  end

  @doc """
  Returns the list of vetted groups.  This should only be used when no user is logged in.

  ## Examples

      iex> list_vetted_groups()
      [%Group{}, ...]

  """
  def list_vetted_groups do
    query =
      from g in Group,
        where: g.vetted == true

    Repo.all(query)
    |> Enum.map(fn group->
      Map.merge(group, %{
        subscribed?: false
      })
    end)

  end

  @doc """
  Returns the list of vetted groups, indicating if current user is subscribed to each.

  ## Examples

      iex> list_vetted_groups(user_id)
      [%Group{}, ...]

  """
  def list_vetted_groups(user_id) do
    query =
      from g in Group,
        where: g.vetted == true

    subscriptions =
      Subscriptions.list_subscriptions_for_user(user_id)
      |> Enum.map(& &1.group_id)

    Repo.all(query)
    |> Enum.map(fn group->
      Map.merge(group, %{
        subscribed?: Enum.member?(subscriptions, group.id)
      })

    end)

  end

  @doc """
  Gets a single group.

  Raises `Ecto.NoResultsError` if the Group does not exist.

  ## Examples

      iex> get_group!(123)
      %Group{}

      iex> get_group!(456)
      ** (Ecto.NoResultsError)

  """
  def get_group!(id), do: Repo.get!(Group, id)

  @doc """
  Creates a group.

  ## Examples

      iex> create_group(%{field: value})
      {:ok, %Group{}}

      iex> create_group(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_group(attrs \\ %{}) do
    %Group{}
    |> Group.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Creates a group and requests vetting.

  ## Examples

      iex> create_group_and_request_vetting(%{field: value})
      {:ok, %Group{}}

      iex> create_group_and_request_vetting(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_group_and_request_vetting(attrs \\ %{}) do
    case create_group(attrs) do
      {:ok, group} ->
        # Automatically subscribe group creator to the group
        Subscriptions.create_subscription(%{
          group_id: group.id,
          user_id: group.creator_id
        })

        # Let's now seek vetting for this group
        VettingRequests.seek_initial_group_vetters(group)

        draft = GroupDrafts.get_draft_for_user(group.creator_id)
        if !is_nil(draft) do
          GroupDrafts.delete_draft(draft)
        end

        {:ok, group}

      {:error, changeset} ->
        {:error, changeset}
    end
  end

  @doc """
  Updates a group.

  ## Examples

      iex> update_group(group, %{field: new_value})
      {:ok, %Group{}}

      iex> update_group(group, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_group(%Group{} = group, attrs) do
    group
    |> Group.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a group.

  ## Examples

      iex> delete_group(group)
      {:ok, %Group{}}

      iex> delete_group(group)
      {:error, %Ecto.Changeset{}}

  """
  def delete_group(%Group{} = group) do
    Repo.delete(group)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking group changes.

  ## Examples

      iex> change_group(group)
      %Ecto.Changeset{data: %Group{}}

  """
  def change_group(%Group{} = group, attrs \\ %{}) do
    Group.changeset(group, attrs)
  end

  @doc """
  Returns count of all unvetted groups for a given user.

  ## Examples

      iex> count_pending_groups_for_user(7)
      2
  """
  def count_pending_groups_for_user(user_id) do
    query =
      from g in Group,
        where: g.creator_id == ^user_id and
          is_nil(g.vetted)

    Repo.aggregate(query, :count)
  end

  def user_has_pending_groups(current_user) do
    if current_user do
      count_pending_groups_for_user(current_user.id) > 0
    else
      false
    end
  end

end
