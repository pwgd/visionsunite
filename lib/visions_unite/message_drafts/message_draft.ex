defmodule VisionsUnite.MessageDrafts.MessageDraft do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Groups.Group

  schema "message_drafts" do
    field :topic_text, :string
    field :message_text, :string

    belongs_to :author, User
    belongs_to :group, Group

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [
      :topic_text,
      :message_text,
      :author_id,
      :group_id,
    ])
    |> validate_required([:author_id, :group_id])
  end
end
