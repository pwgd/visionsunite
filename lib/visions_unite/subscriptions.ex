defmodule VisionsUnite.Subscriptions do
  @moduledoc """
  The Subscriptions context.
  """

  import Ecto.Query, warn: false

  alias VisionsUnite.Repo

  alias VisionsUnite.Subscriptions.Subscription

  @doc """
  Returns the list of subscriptions.

  ## Examples

      iex> list_subscriptions()
      [%Subscription{}, ...]

  """
  def list_subscriptions do
    Repo.all(Subscription)
  end

  @doc """
  Returns a count of subscriptions for a particular group.

  ## Examples

      iex> count_subscriptions_for_group(%Group{})
      32

      iex> count_subscriptions_for_group(482)
      230

  """
  def count_subscriptions_for_group(nil), do: 0

  def count_subscriptions_for_group(group) when is_map(group) do
    query =
      from gs in Subscription,
        where: gs.group_id == ^group.id

    Repo.aggregate(query, :count)
  end

  def count_subscriptions_for_group(group_id) when is_number(group_id) do
    query =
      from gs in Subscription,
        where: gs.group_id == ^group_id

    Repo.aggregate(query, :count)
  end

  @doc """
  Returns the list of subscriptions for a particular group.

  ## Examples

      iex> list_subscriptions_for_group(428)
      [%Subscription{}, ...]

      iex> list_subscriptions_for_group(%Group{})
      [%Subscription{}, ...]
  """
  def list_subscriptions_for_group(nil), do: []

  def list_subscriptions_for_group(group_id) when is_number(group_id) do
    query =
      from gs in Subscription,
        where: gs.group_id == ^group_id

    Repo.all(query)
  end

  def list_subscriptions_for_group(group) when is_map(group) do
    query =
      from gs in Subscription,
        where: gs.group_id == ^group.id

    Repo.all(query)
  end

  @doc """
  Returns the list of subscriptions for a particular user.

  ## Examples

      iex> list_subscriptions_for_user(user_id)
      [%Subscription{}, ...]

  """
  def list_subscriptions_for_user(user_id) do
    query =
      from gs in Subscription,
        where: gs.user_id == ^user_id

    Repo.all(query)
  end

  @doc """
  Gets a single subscription.

  Raises `Ecto.NoResultsError` if the Subscription does not exist.

  ## Examples

      iex> get_subscription!(123)
      %Subscription{}

      iex> get_subscription!(456)
      ** (Ecto.NoResultsError)

  """
  def get_subscription!(id), do: Repo.get!(Subscription, id)

  @doc """
  Gets a single subscription by given group_id and user_id.

  ## Examples

      iex> get_subscription_for_group_and_user(245, 123)
      %Subscription{}

  """
  def get_subscription_for_group_and_user(group_id, user_id) do
    query =
      from gs in Subscription,
        where: gs.group_id == ^group_id and gs.user_id == ^user_id

    Repo.one(query)
  end

  @doc """
  Creates a subscription.

  ## Examples

      iex> create_subscription(%{field: value})
      {:ok, %Subscription{}}

      iex> create_subscription(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_subscription(attrs \\ %{}) do
    %Subscription{}
    |> Subscription.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a subscription.

  ## Examples

      iex> delete_subscription(subscription)
      {:ok, %Subscription{}}

      iex> delete_subscription(subscription)
      {:error, %Ecto.Changeset{}}

  """
  def delete_subscription(%Subscription{} = subscription) do
    Repo.delete(subscription)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking subscription changes.

  ## Examples

      iex> change_subscription(subscription)
      %Ecto.Changeset{data: %Subscription{}}

  """
  def change_subscription(
        %Subscription{} = subscription,
        attrs \\ %{}
      ) do
    Subscription.changeset(subscription, attrs)
  end
end
