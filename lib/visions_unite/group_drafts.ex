defmodule VisionsUnite.GroupDrafts do
  @moduledoc """
  The GroupDrafts context.
  """

import Ecto.Query, warn: false
alias VisionsUnite.Repo

alias VisionsUnite.GroupDrafts.GroupDraft

  @doc """
  Creates a draft.

  ## Examples

      iex> create_draft(%{field: value})
      {:ok, %GroupDraft{}}

      iex> create_draft(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_draft(attrs \\ %{}) do
    %GroupDraft{}
    |> GroupDraft.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a draft.

  ## Examples

      iex> delete_draft(draft)
      {:ok, %GroupDraft{}}

      iex> delete_draft(draft)
      {:error, %Ecto.Changeset{}}

  """
  def delete_draft(%GroupDraft{} = draft) do
    Repo.delete(draft)
  end

  @doc """
  Updates a draft.

  ## Examples

      iex> update_draft(draft, %{field: new_value})
      {:ok, %GroupDraft{}}

      iex> update_draft(draft, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_draft(%GroupDraft{} = draft, attrs) do
    draft
    |> GroupDraft.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Gets a draft (if it exists) for a given user.

  ## Examples

      iex> get_draft_for_user(123)
  %GroupDraft{}

      iex> get_draft_for_user(456)
      nil

  """
  def get_draft_for_user(user_id) do
    query =
      from m in GroupDraft,
      where: m.author_id == ^user_id

    Repo.one(query)
  end
end
