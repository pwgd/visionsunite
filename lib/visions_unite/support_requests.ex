defmodule VisionsUnite.SupportRequests do
  @moduledoc """
  The SupportRequests context.
  """

  import Ecto.Query, warn: false

  alias VisionsUnite.Repo

  alias VisionsUnite.Accounts
  alias VisionsUnite.Messages
  alias VisionsUnite.Messages.Message
  alias VisionsUnite.Topics
  alias VisionsUnite.Groups
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.SupportRequests.SupportRequest
  alias VisionsUnite.Accounts.UserNotifier

  @doc """
  Returns the support sought for a given message and user.

  ## Examples

      iex> get_support_request_for_message_and_user(277, 389)
      [%SupportRequest{}, ...]

  """
  def get_support_request_for_message_and_user(message_id, user_id) do
    query =
      from sr in SupportRequest,
        where:
          sr.message_id == ^message_id and
            sr.user_id == ^user_id

    Repo.one(query)
  end

  def list_support_requests() do
    Repo.all(SupportRequest)
  end

  @doc """
  Returns the list of support requests for a given user.

  ## Examples

      iex> list_support_requests_for_user(user_id)
      [3, 25, ...]

  """
  def list_support_requests_for_user(nil), do: []

  def list_support_requests_for_user(user_id) do
    query =
      from sr in SupportRequest,
        where: sr.user_id == ^user_id

    Repo.all(query)
  end

  @doc """
  Returns the list of support requests for a given message.

  ## Examples

      iex> list_support_requests_for_message(%Message{})
      [3, 25, ...]

  """
  def list_support_requests_for_message(message) do
    query =
      from sr in SupportRequest,
        where: sr.message_id == ^message.id

    Repo.all(query)
  end

  @doc """
  Creates a support_request.

  ## Examples

      iex> create_support_request(user, message, now)
      {:ok, %SupportRequest{}}

  """
  def create_support_request(user, message, now) do
    {:ok, support_request} =
      %SupportRequest{}
      |> SupportRequest.changeset(%{
        user_id: user.id,
        message_id: message.id,
        original_request_date: now
      })
      |> Repo.insert()

    group = Groups.get_group!(message.group_id)
    topic = Topics.get_topic!(message.topic_id)

    UserNotifier.deliver_message_support_request(user, message, group, topic, now, nil)

    SupportRequests.update_support_request(support_request, %{
      last_notified: now
    })

    {:ok, support_request}
  end

  @doc """
  Gets some subset of users in the system and seeks support from them for the given message

  ## Examples

      iex> seek_supporters(%Message{})
      [%User{}, ...]
  """
  def seek_supporters(message) do
    group = Groups.get_group!(message.group_id)
    subscriptions = Subscriptions.list_subscriptions_for_group(group)

    sortition_num =
      subscriptions
      |> Enum.count()
      |> VisionsUnite.Sortition.calculate_sortition_size()

    # TODO save the sortition number in the message row, so that
    #   even if the subscribers count changes, the number of supports
    #   that a message requires for full support isn't affected
    Messages.update_message(message, %{sortition_number: sortition_num})

    now = NaiveDateTime.utc_now()

    # NOTE the -1 is accounting for the author of the
    #   message that is seeking the support
    subscriptions
    |> Enum.reject(&(&1.user_id == message.author_id))
    |> Enum.take_random(Kernel.round(sortition_num - 1))
    |> Enum.each(fn subscription ->
      subscriber = Accounts.get_user!(subscription.user_id)
      # Send the sortition their email / text whatever
      create_support_request(subscriber, message, now)
    end)
  end

  @doc """
  Updates the support_request
  """
  def update_support_request(user, support_request_params) do
    user
    |> SupportRequest.changeset(support_request_params)
    |> Repo.update()
  end

  @doc """
  Deletes a support_request.

  ## Examples

      iex> delete_support_request(support_request)
      {:ok, %SupportRequest{}}

      iex> delete_support_request(support_request)
      {:error, %Ecto.Changeset{}}

  """
  def delete_support_request(%SupportRequest{} = support_request) do
    Repo.delete(support_request)
  end

  @doc """
  Deletes all support_request for a given message.

  ## Examples

      iex> delete_all_support_request_for_message(message)
      {:ok}

  """
  def delete_all_support_request_for_message(%Message{} = message) do
    query =
      from sr in SupportRequest,
        where: sr.message_id == ^message.id

    Repo.delete_all(query)
  end

  def do_preload(support_request, preloads) do
    Repo.preload(support_request, preloads)
  end
end
