defmodule VisionsUnite.MessageDrafts do
  @moduledoc """
  The MessageDrafts context.
  """

import Ecto.Query, warn: false
alias VisionsUnite.Repo

alias VisionsUnite.MessageDrafts.MessageDraft

  @doc """
  Creates a draft.

  ## Examples

      iex> create_draft(%{field: value})
      {:ok, %MessageDraft{}}

      iex> create_draft(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_draft(attrs \\ %{}) do
    %MessageDraft{}
    |> MessageDraft.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a draft.

  ## Examples

      iex> delete_draft(draft)
      {:ok, %MessageDraft{}}

      iex> delete_draft(draft)
      {:error, %Ecto.Changeset{}}

  """
  def delete_draft(%MessageDraft{} = draft) do
    Repo.delete(draft)
  end

  @doc """
  Updates a draft.

  ## Examples

      iex> update_draft(draft, %{field: new_value})
      {:ok, %MessageDraft{}}

      iex> update_draft(draft, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_draft(%MessageDraft{} = draft, attrs) do
    draft
    |> MessageDraft.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Gets a draft (if it exists) for a given user.

  ## Examples

      iex> get_draft_for_user(123)
      %MessageDraft{}

      iex> get_draft_for_user(456)
      nil

  """
  def get_draft_for_user_and_group(user_id, group_id) do
    query =
      from m in MessageDraft,
      where: m.author_id == ^user_id and
      m.group_id == ^group_id

    Repo.one(query)
  end
end
