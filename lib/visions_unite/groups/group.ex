defmodule VisionsUnite.Groups.Group do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Groups

  schema "groups" do
    field :name, :string
    field :description, :string
    field :vetted, :boolean
    field :subscribed?, :boolean, virtual: true

    belongs_to :creator, User

    timestamps()
  end

  @doc false
  def changeset(group, attrs) do
    group
    |> cast(attrs, [:name, :creator_id, :description, :vetted])
    |> validate_required([:name, :creator_id, :description])
    |> unsafe_validate_unique(:name, VisionsUnite.Repo, message: "Cannot create a group that already exists!")
  end

  def is_active?(group) when is_map(group) do
    # We can extend this to indicate that archived or closed-with-prejudice
    # groups are not active.
    group.vetted
  end

  def is_active?(group_id) do
    group = Groups.get_group!(group_id)
    is_active?(group)
  end

end
