defmodule VisionsUnite.Vettings.Vetting do
  use Ecto.Schema
  import Ecto.Changeset
  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Messages.Message
  alias VisionsUnite.Accounts.User

  schema "vettings" do
    field :vet, :boolean

    belongs_to :user, User
    belongs_to :group, Group
    belongs_to :message, Message

    timestamps()
  end

  @doc false
  def changeset(vetting, attrs) do
    vetting
    |> cast(attrs, [:user_id, :group_id, :message_id, :vet])
    |> validate_required([:user_id, :vet])
    |> VisionsUnite.Utilities.validate_one_of()
    |> unique_constraint(:unique_group_vetting, name: :unique_group_vetting)
    |> unique_constraint(:unique_message_vetting, name: :unique_message_vetting)
  end
end
