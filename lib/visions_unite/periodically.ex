defmodule VisionsUnite.Periodically do
  use GenServer

  alias VisionsUnite.Groups
  alias VisionsUnite.Messages
  alias VisionsUnite.VettingRequests
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.Supports
  alias VisionsUnite.Accounts
  alias VisionsUnite.Accounts.UserNotifier
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.Vettings

  @is_late_in_seconds 60 * 60 * elem(Float.parse(System.get_env("IS_LATE_IN_HOURS", "48")), 0)
  @reminder_interval_in_seconds 60 * 60 * elem(Float.parse(System.get_env("REMINDER_INTERVAL_IN_HOURS", "24")), 0)
  @rotation_needed_in_seconds 60 * 60 * elem(Float.parse(System.get_env("ROTATION_NEEDED_IN_HOURS", "96")), 0)
  @work_frequency_in_milliseconds 1000 * elem(Integer.parse(System.get_env("WORK_FREQUENCY", "180")), 0)

  def start_link(_opts) do
    GenServer.start_link(__MODULE__, %{})
  end

  def init(state) do
    schedule_work()
    {:ok, state}
  end

  def handle_info(:work, state) do
    now = NaiveDateTime.utc_now()

    vetting_rotations_needed =
      VettingRequests.list_vetting_requests()
      |> Enum.filter(fn vetting_request ->
        rotation_is_needed?(now, vetting_request)
      end)

    # Determine any necessary rotations for vetting requests
    vetting_rotations_needed
    |> Enum.group_by(fn vetting_request ->
      # group by either group_id or message_id
      if vetting_request.group_id do
        vetting_request.group_id
      else
        vetting_request.message_id
      end
    end)
    |> Enum.each(fn {group_or_message_id, old_vetting_requests} ->
      group_or_message =
        if !is_nil(List.first(old_vetting_requests).group_id) do
          "group"
        else
          "message"
        end

      group =
        if group_or_message == "group" do
          Groups.get_group!(group_or_message_id)
        end

      message =
        if group_or_message == "message" do
          Messages.get_message!(group_or_message_id)
        end

      new_vetting_list =
        # Get list of all vetters
        Accounts.list_vetters()
        |> Enum.filter(fn user ->
          # Filter out the author / creator of this message / group
          case group_or_message do
            "group" ->
              user.id !== group.creator_id
            "message" ->
              user.id !== message.author_id
          end
        end)
        |> Enum.reject(fn user ->
          # Exclude any vetter that is present in this list of awols
          Enum.any?(old_vetting_requests, fn vetting_request ->
            vetting_request.user_id == user.id
          end)
        end)
        |> Enum.reject(fn user ->
          # Exclude any vetter that is unresponsive
          user.declined_to_participate > System.get_env("UNRESPONSIVE_THRESHOLD", "50")
        end)
        |> Enum.reject(fn user ->
          # Exclude any vetter that has already responded to this request
          case group_or_message do
            "group" ->
              Enum.any?(Vettings.list_vettings(), fn vetting ->
                vetting.user_id == user.id && vetting.group_id == group.id
              end)
            "message" ->
              Enum.any?(Vettings.list_vettings(), fn vetting ->
                vetting.user_id == user.id && vetting.message_id == message.id
              end)
          end
        end)
        |> Enum.take_random(Enum.count(old_vetting_requests))

      # send new vetting requests,
      # delete old vetting requests,
      # and send notifications
      old_vetting_requests
      |> Enum.zip(new_vetting_list)
      |> Enum.each(fn {old_vetting_request, new_vetter} ->
        case group_or_message do
          "group" ->
            group = Groups.get_group!(old_vetting_request.group_id)
            VettingRequests.create_group_vetting_request(new_vetter, group, now, old_vetting_request.original_request_date)
          "message" ->
            message = Messages.get_message!(old_vetting_request.message_id)
            VettingRequests.create_message_vetting_request(new_vetter, message, now, old_vetting_request.original_request_date)
        end

        VettingRequests.delete_vetting_request(old_vetting_request)

        old_user = Accounts.get_user!(old_vetting_request.user_id)
        Accounts.update_user_decline_counter(old_user, :increment)

        UserNotifier.deliver_vetting_cancellation_notification(
          old_user,
          old_vetting_request
        )
      end)
    end)

    # Determine any necessary rotations for support requests
    support_rotations_needed =
      SupportRequests.list_support_requests()
      |> Enum.filter(fn support_request ->
        rotation_is_needed?(now, support_request)
      end)

    support_rotations_needed
    |> Enum.group_by(fn support_request ->
      # group by message_id
      support_request.message_id
    end)
    |> Enum.each(fn {message_id, old_support_requests} ->
      message =
        Messages.get_message!(message_id)
        |> Messages.do_preload([:group])

      new_support_list =
        # Get list of subscribers of this group
        Subscriptions.list_subscriptions_for_group(message.group)
        |> Enum.map(fn subscription ->
          Accounts.get_user!(subscription.user_id)
        end)
        |> Enum.reject(fn user ->
          # Exclude anyone that is present in this list of awols
          Enum.any?(old_support_requests, fn support_request ->
            support_request.user_id == user.id
          end)
        end)
        |> Enum.reject(fn user ->
          # Exclude any vetter that is unresponsive
          user.declined_to_participate > System.get_env("UNRESPONSIVE_THRESHOLD", "50")
        end)
        |> Enum.reject(fn user ->
          # Exclude anyone that has already responded to this request
          Enum.any?(Supports.list_supports(), fn support ->
            support.user_id == user.id && support.message_id == message.id
          end)
        end)
        |> Enum.take_random(Enum.count(old_support_requests))

      old_support_requests
      |> Enum.each(fn old_support_request ->
        user = Accounts.get_user!(old_support_request.user_id)

        UserNotifier.deliver_message_support_cancellation_notification(
          user,
          message
        )

        SupportRequests.delete_support_request(old_support_request)
        Accounts.update_user_decline_counter(user, :increment)
      end)

      new_support_list
      |> Enum.each(fn user ->
        SupportRequests.create_support_request(user, message, now)
      end)
    end)

    # Then, detect any late requests and
    # send reminder if diff(now, last_reminded) >= reminder_interval_in_hours (or null).
    #
    # If diff(now, original_request_date) >= @rotate_in_seconds,
    # no worries, the template gives final warning

    late_vetting_requests =
      VettingRequests.list_vetting_requests()
      |> Enum.filter(fn vetting_request ->
        is_late?(now, vetting_request)
      end)

    late_support_requests =
      SupportRequests.list_support_requests()
      |> Enum.filter(fn support_request ->
        is_late?(now, support_request)
      end)

    # Deliver vetting request reminders
    late_vetting_requests
    |> Enum.map(& VettingRequests.do_preload(&1, [:user, :message, :group]))
    |> Enum.each(fn vetting_request ->
      if send_reminder?(now, vetting_request) do
        if vetting_request.message_id do
          message =
            vetting_request.message
            |> Messages.do_preload([:topic, :group])

          UserNotifier.deliver_message_vetting_request(
            vetting_request.user,
            vetting_request.message,
            message.group,
            message.topic,
            vetting_request.original_request_date,
            vetting_request.last_notified
          )

          VettingRequests.update_vetting_request(vetting_request, %{
            last_notified: now
          })
        end

        if vetting_request.group_id do
          UserNotifier.deliver_group_vetting_request(
            vetting_request.user,
            vetting_request.group,
            vetting_request.original_request_date,
            vetting_request.last_notified
          )

          VettingRequests.update_vetting_request(vetting_request, %{
            last_notified: now
          })
        end
      end
    end)

    # Deliver support request reminders
    late_support_requests
    |> Enum.map(& SupportRequests.do_preload(&1, [:user, :message]))
    |> Enum.each(fn support_request ->
      if send_reminder?(now, support_request) do
        message =
          support_request.message
          |> Messages.do_preload([:topic, :group])

        UserNotifier.deliver_message_support_request(
          support_request.user,
          message,
          message.group,
          message.topic,
          support_request.original_request_date,
          support_request.last_notified
        )

        SupportRequests.update_support_request(support_request, %{
          last_notified: now
        })
      end
    end)

    schedule_work()

    {:noreply, state}
  end

  defp schedule_work() do
    Process.send_after(self(), :work, @work_frequency_in_milliseconds) # Repeat work on a schedule as defined by the WORK_FREQUENCY environment variable
  end

  defp send_reminder?(now, request) do
    if is_late?(now, request) && is_nil(request.last_notified) do
      # If reminder has never been sent, and the request is late, send a reminder
      true
    else
      # If it has been @reminder_interval_in_seconds since the last notification,
      #   send another reminder
      NaiveDateTime.diff(now, request.last_notified) >= @reminder_interval_in_seconds
    end
  end

  defp is_late?(now, request) do
    NaiveDateTime.diff(now, request.original_request_date) >= @is_late_in_seconds
  end

  defp rotation_is_needed?(now, request) do
    NaiveDateTime.diff(now, request.latest_request_date) >= @rotation_needed_in_seconds and NaiveDateTime.diff(now, request.original_request_date) < @rotation_needed_in_seconds * 2.5
  end
end
