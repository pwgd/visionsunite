defmodule VisionsUnite.SupportRequests.SupportRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Messages.Message

  schema "support_requests" do
    field :original_request_date, :naive_datetime
    field :last_notified, :naive_datetime

    belongs_to :user, User
    belongs_to :message, Message

    timestamps()
  end

  @doc false
  def changeset(support_request, attrs) do
    support_request
    |> cast(attrs, [:user_id, :message_id, :original_request_date, :last_notified])
    |> validate_required([:user_id, :message_id, :original_request_date])
    |> unique_constraint(:unique_support_request, name: :unique_support_request)
  end
end
