defmodule VisionsUnite.VettingRequests do
  @moduledoc """
  The VettingRequests context.
  """

  import Ecto.Query, warn: false

  alias VisionsUnite.Repo

  alias VisionsUnite.Accounts
  alias VisionsUnite.Topics
  alias VisionsUnite.Groups
  alias VisionsUnite.VettingRequests
  alias VisionsUnite.VettingRequests.VettingRequest
  alias VisionsUnite.Accounts.UserNotifier

  @doc """
  Returns the vetting sought for a given group and user.

  ## Examples

      iex> get_vetting_request_for_group_and_user!(289, 297)
      [%VettingRequest{}, ...]

  """
  def get_vetting_request_for_group_and_user(group_id, user_id) do
    query =
      from vr in VettingRequest,
        where: vr.group_id == ^group_id and vr.user_id == ^user_id

    Repo.one(query)
  end

  @doc """
  Returns the vetting sought for a given message and user.

  ## Examples

      iex> get_vetting_request_for_message_and_user!(672, 297)
      [%VettingRequest{}, ...]

  """
  def get_vetting_request_for_message_and_user(message_id, user_id) do
    query =
      from vr in VettingRequest,
        where: vr.message_id == ^message_id and vr.user_id == ^user_id

    Repo.one(query)
  end

  def list_vetting_requests() do
    Repo.all(VettingRequest)
  end

  @doc """
  Returns the list of vetting sought for a given user.

  ## Examples

      iex> list_vetting_sought_for_user(user_id)
      [3, 25, ...]

  """
  def list_vetting_sought_for_user(nil), do: []

  def list_vetting_sought_for_user(user_id) do
    query =
      from vr in VettingRequest,
        where: vr.user_id == ^user_id

    Repo.all(query)
  end

  @doc """
  Returns the list of vetting sought for a given message.

  ## Examples

      iex> list_vetting_sought_for_message(%Message{})
      [3, 25, ...]

  """
  def list_vetting_sought_for_message(message) do
    query =
      from vr in VettingRequest,
        where: vr.message_id == ^message.id

    Repo.all(query)
  end

  @doc """
  Creates a vetting_request for a message.

  ## Examples

      iex> create_message_vetting_request(user, message, now, original)
      {:ok, %VettingRequest{}}

  """
  def create_message_vetting_request(user, message, now, original) do
    {:ok, vetting_request} =
      %VettingRequest{}
      |> VettingRequest.changeset(%{
        user_id: user.id,
        message_id: message.id,
        original_request_date: original,
        latest_request_date: now
      })
      |> Repo.insert()

    group = Groups.get_group!(message.group_id)
    topic = Topics.get_topic!(message.topic_id)

    UserNotifier.deliver_message_vetting_request(user, message, group, topic, now, nil)

    VettingRequests.update_vetting_request(vetting_request, %{
      last_notified: now
    })

    {:ok, vetting_request}
  end

  @doc """
  Creates a vetting_request for a group.

  ## Examples

      iex> create_group_vetting_request(user, group, now, original)
      {:ok, %VettingRequest{}}

  """
  def create_group_vetting_request(user, group, now, original) do
    {:ok, vetting_request} =
      %VettingRequest{}
      |> VettingRequest.changeset(%{
        user_id: user.id,
        group_id: group.id,
        original_request_date: original,
        latest_request_date: now
      })
      |> Repo.insert()

    UserNotifier.deliver_group_vetting_request(user, group, now, nil)

    VettingRequests.update_vetting_request(vetting_request, %{
      last_notified: now
    })

    {:ok, vetting_request}
  end

  @doc """
  Gets some subset of users in the system and seeks vetting from them for the given message

  ## Examples

      iex> seek_vetters(%Message{})
      [%User{}, ...]
  """
  def seek_initial_message_vetters(message) do
    vetters = Accounts.list_vetters()

    now = NaiveDateTime.utc_now()

    vetters
    |> Enum.filter(fn user ->
      user.id !== message.author_id
    end)
    |> Enum.take_random(Kernel.round(String.to_integer(System.get_env("VETTING_REACHED") || "5")) * 2)
    |> Enum.each(fn user ->
      create_message_vetting_request(user, message, now, now)
    end)
  end

  @doc """
  Gets some subset of users in the system and seeks vetting from them for the given group

  ## Examples

      iex> seek_vetters(%Group{})
      [%User{}, ...]
  """
  def seek_initial_group_vetters(group) do
    vetters = Accounts.list_vetters()

    now = NaiveDateTime.utc_now()

    vetters
    |> Enum.filter(fn user ->
      user.id !== group.creator_id
    end)
    |> Enum.take_random(Kernel.round(String.to_integer(System.get_env("VETTING_REACHED") || "5")) * 2)
    |> Enum.each(fn user ->
      create_group_vetting_request(user, group, now, now)
    end)
  end

  @doc """
  Deletes a vetting_request.

  ## Examples

      iex> delete_vetting_request(vetting_request)
      {:ok, %VettingRequest{}}

      iex> delete_vetting_request(vetting_request)
      {:error, %Ecto.Changeset{}}

  """
  def delete_vetting_request(%VettingRequest{} = vetting_request) do
    Repo.delete(vetting_request)
  end

  @doc """
  Deletes all vetting_requests for a given message.

  ## Examples

      iex> delete_all_vetting_requests_for_message(729)
      {:ok}

  """
  def delete_all_vetting_requests_for_message(message_id) do
    query =
      from vr in VettingRequest,
        where: vr.message_id == ^message_id

    Repo.delete_all(query)
  end

  @doc """
  Updates the vetting_request
  """
  def update_vetting_request(user, vetting_request_params) do
    user
    |> VettingRequest.changeset(vetting_request_params)
    |> Repo.update()
  end

  @doc """
  Deletes all vetting_requests for a given group.

  ## Examples

      iex> delete_all_vetting_requests_for_group(2839)
      {:ok}

  """
  def delete_all_vetting_requests_for_group(group_id) do
    query =
      from vr in VettingRequest,
        where: vr.group_id == ^group_id

    Repo.delete_all(query)
  end

  def do_preload(vetting_request, preloads) do
    Repo.preload(vetting_request, preloads)
  end
end
