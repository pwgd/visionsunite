defmodule VisionsUnite.Supports.Support do
  use Ecto.Schema
  import Ecto.Changeset
  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Messages.Message

  schema "supports" do
    field :support, :boolean

    belongs_to :user, User
    belongs_to :message, Message

    timestamps()
  end

  @doc false
  def changeset(supporting, attrs) do
    supporting
    |> cast(attrs, [:user_id, :message_id, :support])
    |> validate_required([:user_id, :message_id, :support])
    |> unique_constraint(:unique_support, name: :unique_support)
  end
end
