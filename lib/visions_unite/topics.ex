defmodule VisionsUnite.Topics do
  @moduledoc """
  The Topics context.
  """

  import Ecto.Query, warn: false

  alias VisionsUnite.Repo
  alias VisionsUnite.Topics.Topic

  @doc """
  Returns the list of topics.

  ## Examples

      iex> list_topics()
      [%Topic{}, ...]

  """
  def list_topics do
    Repo.all(Topic)
  end

  @doc """
  Returns the list of topics (of fully supported messagse) for a given group.

  ## Examples

      iex> list_topics_for_group(group_id)
      [%Topic{}, ...]

  """
  def list_topics_for_group(group_id) do

    query =
      from t in Topic,
        where:
          t.group_id == ^group_id and
            t.vetted == true

    Repo.all(query)
  end

  @doc """
  Gets a single topic.

  Raises `Ecto.NoResultsError` if the Topic does not exist.

  ## Examples

      iex> get_topic!(123)
      %Topic{}

      iex> get_topic!(456)
      ** (Ecto.NoResultsError)

  """
  def get_topic!(id), do: Repo.get!(Topic, id)

  @doc """
  Gets a single topic by group and name.

  ## Examples

      iex> get_topic_by_group_and_name(123, "some name")
      %Topic{}
  """
  def get_topic_by_group_and_name(nil, _name), do: nil
  def get_topic_by_group_and_name(_group_id, nil), do: nil

  def get_topic_by_group_and_name(group_id, name) do
    query =
      from t in Topic,
        where:
          t.group_id == ^group_id and
            t.name == ^name

    Repo.one(query)
  end

  @doc """
  Creates a topic.

  ## Examples

      iex> create_topic(%{field: value})
      {:ok, %Topic{}}

      iex> create_topic(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_topic(attrs \\ %{}) do
    %Topic{}
    |> Topic.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking topic changes.

  ## Examples

      iex> change_topic(topic)
      %Ecto.Changeset{data: %Topic{}}

  """
  def change_topic(%Topic{} = topic, attrs \\ %{}) do
    Topic.changeset(topic, attrs)
  end

  @doc """
  Updates a topic.

  ## Examples

      iex> update_topic(topic, %{field: new_value})
      {:ok, %Topic{}}

      iex> update_topic(topic, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_topic(%Topic{} = topic, attrs) do
    topic
    |> Topic.changeset(attrs)
    |> Repo.update()
  end
end
