defmodule VisionsUnite.Utilities do
  import Ecto.Changeset

  def validate_one_of(changeset) do
    group_id = get_field(changeset, :group_id)
    message_id = get_field(changeset, :message_id)

    case is_nil(group_id) && is_nil(message_id) do
      true ->
        add_error(changeset, :group_id, "must have either group or message associated")
        add_error(changeset, :message_id, "must have either group or message associated")

      _ ->
        changeset
    end
  end

  def truncate(nil, _), do: ""
  def truncate(_, nil), do: ""

  def truncate(string, n) do
    string = string|> HtmlSanitizeEx.strip_tags()
    if String.length(string) < n do
      string
    else
      "#{String.slice(string, 0..n)} ..."
    end
  end
end
