defmodule VisionsUnite.Topics.Topic do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Groups.Group

  schema "topics" do
    field :name, :string
    field :vetted, :boolean

    belongs_to :group, Group

    timestamps()
  end

  @doc false
  def changeset(topic, attrs) do
    topic
    |> cast(attrs, [:name, :group_id, :vetted])
    |> validate_required([:name, :group_id])
    |> unsafe_validate_unique([:name, :group_id], VisionsUnite.Repo, message: "Cannot create a topic that already exists in this group!")
  end
end
