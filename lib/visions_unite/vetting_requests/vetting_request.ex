defmodule VisionsUnite.VettingRequests.VettingRequest do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User
  alias VisionsUnite.Messages.Message
  alias VisionsUnite.Groups.Group

  schema "vetting_requests" do
    field :original_request_date, :naive_datetime
    field :last_notified, :naive_datetime
    field :latest_request_date, :naive_datetime

    belongs_to :user, User
    belongs_to :message, Message
    belongs_to :group, Group

    timestamps()
  end

  @doc false
  def changeset(vetting_request, attrs) do
    vetting_request
    |> cast(attrs, [:user_id, :message_id, :group_id, :original_request_date, :last_notified, :latest_request_date])
    |> validate_required([:user_id, :original_request_date, :latest_request_date])
    |> VisionsUnite.Utilities.validate_one_of()
    |> unique_constraint(:unique_group_vetting_request, name: :unique_group_vetting_request)
    |> unique_constraint(:unique_message_vetting_request, name: :unique_message_vetting_request)
  end
end
