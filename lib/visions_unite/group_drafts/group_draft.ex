defmodule VisionsUnite.GroupDrafts.GroupDraft do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Accounts.User

  schema "group_drafts" do
    field :name_text, :string
    field :description_text, :string

    belongs_to :author, User

    timestamps()
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [
      :name_text,
      :description_text,
      :author_id,
    ])
    |> validate_required([:author_id])
  end
end
