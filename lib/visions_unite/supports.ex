defmodule VisionsUnite.Supports do
  @moduledoc """
  The Supports context.
  """

  import Ecto.Query, warn: false
  alias VisionsUnite.Repo

  alias VisionsUnite.Accounts
  alias VisionsUnite.Supports.Support
  alias VisionsUnite.SupportRequests
  alias VisionsUnite.Groups
  alias VisionsUnite.Messages
  alias VisionsUnite.Subscriptions
  alias VisionsUnite.Topics
  alias VisionsUnite.NewNotifications
  alias VisionsUnite.Accounts.UserNotifier

  @doc """
  Returns the list of supports.

  ## Examples

      iex> list_supports()
      [%Support{}, ...]

  """
  def list_supports do
    Repo.all(Support)
  end

  @doc """
  Returns a count of supports for a particular message.

  ## Examples

      iex> count_supports_for_message(%Message{})
      32

      iex> count_supports_for_message(482)
      230

  """
  def count_supports_for_message(nil), do: 0

  def count_supports_for_message(message) when is_map(message) do
    query =
      from s in Support,
        where:
          s.message_id == ^message.id and
            s.support == true

    Repo.aggregate(query, :count)
  end

  def count_supports_for_message(message_id) do
    query =
      from s in Support,
        where:
          s.message_id == ^message_id and
            s.support == true

    Repo.aggregate(query, :count)
  end

  @doc """
  This function returns the list of supports == true for a given message.

  ## Examples

      iex> list_supports_for_message(message)
      [%Support{} ...]

  """
  def list_supports_for_message(message) do
    query =
      from s in Support,
        where:
          s.message_id == ^message.id and
            s.support == true

    Repo.all(query)
  end

  @doc """
  This function returns the list of all supports (even support == false) for a given message.

  ## Examples

      iex> list_all_supports_for_message(message)
      [%Support{} ...]

  """
  def list_all_supports_for_message(message) do
    query =
      from s in Support,
        where: s.message_id == ^message.id

    Repo.all(query)
  end

  @doc """
  Gets a single support.

  Raises `Ecto.NoResultsError` if the Support does not exist.

  ## Examples

      iex> get_support!(123)
      %Support{}

      iex> get_support!(456)
      ** (Ecto.NoResultsError)

  """
  def get_support!(id), do: Repo.get!(Support, id)

  @doc """
  Creates a support.

  ## Examples

      iex> create_support(%{field: value})
      {:ok, %Support{}}

      iex> create_support(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_support(attrs \\ %{}) do
    message_id = attrs["message_id"]
    user_id = attrs["user_id"]
    message = Messages.get_message!(message_id)
    group = Groups.get_group!(message.group_id)
    topic = Topics.get_topic!(message.topic_id)

    support =
      %Support{}
      |> Support.changeset(attrs)
      |> Repo.insert()

    # Remove the existing support request from the user that just clicked
    # NOTE! the support request might be state...
    existing_support_request =
      SupportRequests.get_support_request_for_message_and_user(message_id, user_id)

    if !is_nil(existing_support_request) do
      SupportRequests.delete_support_request(existing_support_request)

      # Support Request has been deleted.... now we check for support
      Subscriptions.count_subscriptions_for_group(group)

      # NOTE: this sortition number was saved at message creation (seek_supporters)
      #   It is based off the number of subscribers of a group at the time of
      #   message creation, so that if the number of subscribers increases,
      #   it doesn't unnecessarily "penalize" the in-progress support count.
      sortition_num = message.sortition_number
      quorum_size = Kernel.round(sortition_num * 0.51)
      support_size = count_supports_for_message(message)

      if support_size >= quorum_size do
        # if message has been fully supported, remove all support requests
        # for this message because the goal has already been reached
        SupportRequests.delete_all_support_request_for_message(message)

        # set the message as fully supported
        Messages.update_message(message, %{fully_supported: true})

        # also, create "new notifications" for the subscribers of this group
        Subscriptions.list_subscriptions_for_group(group)
        |> Enum.each(fn subscription ->
          NewNotifications.create_new_notification(%{
            message_id: message.id,
            user_id: subscription.user_id
          })
          recipient_user = Accounts.get_user!(subscription.user_id)
          UserNotifier.deliver_message_support_announcement(recipient_user, message, group, topic)
        end)
      end

      # return the support
      support
    else
      {:error, :no_existing_support_request}
    end
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking support changes.

  ## Examples

      iex> change_support(support)
      %Ecto.Changeset{data: %Support{}}

  """
  def change_support(%Support{} = support, attrs \\ %{}) do
    Support.changeset(support, attrs)
  end
end
