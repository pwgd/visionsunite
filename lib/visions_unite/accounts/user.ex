defmodule VisionsUnite.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset

  schema "users" do
    field :email, :string
    field :vetter, :boolean, default: true
    field :password, :string, virtual: true, redact: true
    field :hashed_password, :string, redact: true
    field :confirmed_at, :naive_datetime
    field :super_admin, :boolean, default: false
    field :phone_number, :string
    field :receive_texts, :boolean, default: false
    field :receive_emails, :boolean, default: true
    field :declined_to_participate, :integer, default: 0

    timestamps()
  end

  @doc """
  A user changeset for registration.

  It is important to validate the length of both email and password.
  Otherwise databases may truncate the email without warnings, which
  could lead to unpredictable or insecure behaviour. Long passwords may
  also be very expensive to hash for certain algorithms.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def registration_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:email, :password, :vetter, :super_admin, :phone_number])
    |> validate_email()
    |> validate_password(opts)
    |> validate_phone_number()
    |> clean_phone_number()
    |> validate_phone_length()
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, VisionsUnite.Repo)
    |> unique_constraint(:email)
  end

  defp validate_password(changeset, opts) do
    changeset
    |> validate_required([:password])
    |> validate_length(:password, min: 8, max: 72)
    # |> validate_format(:password, ~r/[a-z]/, message: "at least one lower case character")
    # |> validate_format(:password, ~r/[A-Z]/, message: "at least one upper case character")
    # |> validate_format(:password, ~r/[!?@#$%^&*_0-9]/, message: "at least one digit or punctuation character")
    |> maybe_hash_password(opts)
  end

  defp validate_phone_number(changeset) do
    changeset
    |> validate_format(:phone_number, ~r/^[\d|\(|\)|\-|\+|\s]+$/, message: "must be only numbers or valid phone number symbols")
  end

  defp validate_phone_length(changeset) do
    phone_number = get_field(changeset, :phone_number)

    number_length_message = "phone number must be 10 digits (or 11 digits if \"1\" is pre-pended)."

    if !is_nil(phone_number) do
      if String.first(phone_number) == "1" do
        changeset
        |> validate_length(:phone_number, is: 11, message: number_length_message)
      else
        changeset
        |> validate_length(:phone_number, is: 10, message: number_length_message)
      end
    else
      changeset
    end
  end

  defp maybe_hash_password(changeset, opts) do
    hash_password? = Keyword.get(opts, :hash_password, true)
    password = get_change(changeset, :password)

    if hash_password? && password && changeset.valid? do
      changeset
      # If using Bcrypt, then further validate it is at most 72 bytes long
      |> validate_length(:password, max: 72, count: :bytes)
      |> put_change(:hashed_password, Bcrypt.hash_pwd_salt(password))
      |> delete_change(:password)
    else
      changeset
    end
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> cast(attrs, [:receive_emails])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{changes: %{receive_emails: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  @doc """
  A user changeset for changing notification settiongs.
  """
  def notification_changeset(user, attrs) do
    user
    |> cast(attrs, [:receive_texts])
    |> cast(attrs, [:receive_emails])
  end

  @doc """
  A user changeset for changing the password.

  ## Options

    * `:hash_password` - Hashes the password so it can be stored securely
      in the database and ensures the password field is cleared to prevent
      leaks in the logs. If password hashing is not needed and clearing the
      password field is not desired (like when using this changeset for
      validations on a LiveView form), this option can be set to `false`.
      Defaults to `true`.
  """
  def password_changeset(user, attrs, opts \\ []) do
    user
    |> cast(attrs, [:password])
    |> validate_confirmation(:password, message: "does not match password")
    |> validate_password(opts)
  end

  def vetter_changeset(user, attrs) do
    user
    |> cast(attrs, [:vetter])
  end

  def declined_to_participate_changeset(user, attrs) do
    user
    |> cast(attrs, [:declined_to_participate])
  end

  def phone_changeset(user, attrs) do
    user
    |> cast(attrs, [:phone_number, :receive_texts])
    |> validate_phone_number()
    |> clean_phone_number()
    |> validate_phone_length()
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end

  @doc """
  Verifies the password.

  If there is no user or the user doesn't have a password, we call
  `Bcrypt.no_user_verify/0` to avoid timing attacks.
  """
  def valid_password?(%VisionsUnite.Accounts.User{hashed_password: hashed_password}, password)
      when is_binary(hashed_password) and byte_size(password) > 0 do
    Bcrypt.verify_pass(password, hashed_password)
  end

  def valid_password?(_, _) do
    Bcrypt.no_user_verify()
    false
  end

  @doc """
  Validates the current password otherwise adds an error to the changeset.
  """
  def validate_current_password(changeset, password) do
    if valid_password?(changeset.data, password) do
      changeset
    else
      add_error(changeset, :current_password, "is not valid")
    end
  end

  @doc """
  Stores the phone number as digits only
  """
  def clean_phone_number(changeset) do
    dirty_phone = get_field(changeset, :phone_number)

    if is_nil(dirty_phone) do
      changeset
    else
      clean_phone =
        dirty_phone
        |> String.replace(~r/[^\d]/, "")

      put_change(changeset, :phone_number, clean_phone)
    end
  end
end
