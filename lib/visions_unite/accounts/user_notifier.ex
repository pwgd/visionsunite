# TODO Check TWILIO_NUMBER or EMAIL_FROM_ADDR before attempting to send.
defmodule VisionsUnite.Accounts.UserNotifier do
  import Swoosh.Email

  alias VisionsUnite.Accounts
  alias VisionsUnite.Mailer
  alias VisionsUnite.Groups
  alias VisionsUnite.Topics
  alias VisionsUnite.Messages

  @final_reminder_in_seconds 60 * 60 * elem(Float.parse(System.get_env("FINAL_REMINDER_IN_HOURS", "77")), 0)

  # Sends the SMS using ExTwilio
  defp deliver_sms(user, body) do
    if !is_nil(user.phone_number) do
      Task.Supervisor.start_child(VisionsUnite.Supervisor, fn ->
        ExTwilio.Message.create(
          to: user.phone_number,
          from: System.get_env("TWILIO_NUMBER"),
          body: body
        )
      end)
    end
  end

  # Delivers the email using the application mailer.
  defp deliver_email(user, subject, body) do
    Task.Supervisor.start_child(VisionsUnite.Supervisor, fn ->
      email =
        new()
        |> to(user.email)
        |> from({System.get_env("EMAIL_FROM_TEXT"), System.get_env("EMAIL_FROM_ADDR")})
        |> subject(subject)
        |> text_body(body)

      with {:ok, _metadata} <- Mailer.deliver(email) do
        {:ok, email}
      else
        anything_else ->
          IO.puts "something went wrong: #{inspect anything_else}"
      end
    end)
  end

  @doc """
  Deliver instructions to confirm account.
  """
  def deliver_confirmation_instructions(user, url) do
    deliver_email(user, "Confirmation instructions", """

    ==============================

    Hi #{user.email},

    Please confirm your account by visiting the link below:

    #{url}

    If you did not create an account with Visions Unite, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to reset a user password.
  """
  def deliver_reset_password_instructions(user, url) do
    deliver_email(user, "Reset password instructions", """

    ==============================

    Hi #{user.email},

    You can reset your password by visiting the link below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver instructions to update a user email.
  """
  def deliver_update_email_instructions(user, url) do
    deliver_email(user, "Update email instructions", """

    ==============================

    Hi #{user.email},

    You can change your email by visiting the URL below:

    #{url}

    If you didn't request this change, please ignore this.

    ==============================
    """)
  end

  @doc """
  Deliver request to vet group.
  """
  def deliver_group_vetting_request(user, group, original_request_date, last_notified) do
    now = NaiveDateTime.utc_now()

    first = is_nil(last_notified)

    final_reminder = NaiveDateTime.diff(now, original_request_date) > @final_reminder_in_seconds

    if user.receive_emails do
      subject =
        cond do
          first ->
            "[Visions Unite] Your assistance is requested! (Group Vetting)"
          final_reminder ->
            "[Visions Unite] Final Reminder! Your assistance is requested! (Group Vetting)"
          true ->
            "[Visions Unite] Reminder: Your assistance is requested! (Group Vetting)"
        end

      body =
        cond do
          first ->
            """
            Someone has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            Please visit the URL below:

            #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id}

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          final_reminder ->
            """
            Final reminder! Someone has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id} and make your voice heard as soon as possible!

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          true ->
            """
            Just a reminder that has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id} and make your voice heard.

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_email(user, subject, body)
    end

    if user.receive_texts do
      body =
        cond do
          first ->
            """
            Someone has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            At your convenience, please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id} and make your voice heard.

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """

          final_reminder ->
            """
            This is the final reminder! Someone has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id}

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """

          true ->
            """
            This is a reminder that someone has proposed the following group:

            Name: #{group.name}

            Description: #{group.description}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this group should exist on the site, please vet this group. Likewise if you do not think this group is likely to produce relevant and important messages, please reject this group.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/group/#{group.id}

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_sms(user, body)
    end
  end

  @doc """
  Deliver request to vet message.
  """
  def deliver_message_vetting_request(user, message, group, topic, original_request_date, last_notified) do
    now = NaiveDateTime.utc_now()

    first = is_nil(last_notified)

    final_reminder = NaiveDateTime.diff(now, original_request_date) > @final_reminder_in_seconds

    if user.receive_emails do
      subject =
        cond do
          first ->
            "[Visions Unite] Your assistance is requested! (Message Vetting)"
          final_reminder ->
            "[Visions Unite] Last chance to vet message!"
          true ->
            "[Visions Unite] Reminder! Your assistance is requested! (Message Vetting)"
        end

      body =
        cond do
          first ->
            """
            Someone has proposed the following message for the group #{group.name} under the topic #{topic.name}

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            At your convenience, please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard.

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          final_reminder ->
            """
            This is your last chance to vet the following new message!

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard as soon as possible. You have one more day!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          true ->
            """
            Reminder! Someone has proposed the following message for the group #{group.name} under the topic #{topic.name}

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard!

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_email(user, subject, body)
    end

    if user.receive_texts do
      body =
        cond do
          first ->
            """
            Someone has proposed the following message for the group #{group.name} under the topic #{topic.name}

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            At your convenience, please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard.

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """

          final_reminder ->
            """
            Final reminder that someone has proposed the following message for the group #{group.name} under the topic #{topic.name}

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard as soon as possible!

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """

          true ->
            """
            A reminder that someone has proposed the following message for the group #{group.name} under the topic #{topic.name}

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should exist on the site, please vet this message. Likewise if you think this message is likely spam, misinformation, or otherwise harmful, please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/vettings/message/#{message.id} and make your voice heard.

            The Visions Unite community appreciates your efforts!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_sms(user, body)
    end
  end

  @doc """
  Deliver request to support message.
  """
  def deliver_message_support_request(user, message, group, topic, original_request_date, last_notified) do
    now = NaiveDateTime.utc_now()

    first = is_nil(last_notified)

    final_reminder = NaiveDateTime.diff(now, original_request_date) > @final_reminder_in_seconds

    if user.receive_emails do
      subject =
        cond do
          first ->
            "[Visions Unite] Your assistance is requested! (Supporting a message)"
          final_reminder->
            "[Visions Unite] Last chance to support message!"
          true ->
            "[Visions Unite] Reminder! Your assistance is requested! (Supporting a message)"
        end

      body =
        cond do
          first ->
            """
            Someone has proposed the following message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            At your convenience, please log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard.

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          final_reminder ->
            """
            This is your last chance to support the following message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            You have one more day to log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard.

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          true ->
            """
            As a reminder, someone has proposed the following message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard. But don't take too long!

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration.

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_email(user, subject, body)
    end

    if user.receive_texts do
      body =
        cond do
          first ->
            """
            Someone has proposed the following message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            At your convenience, please log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard.

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration!

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          final_reminder ->
            """
            This is your last chance to support the following new message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard. You have one more day!

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration.

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
          true ->
            """
            As a reminder, someone has proposed the following message:

            #{message.text}

            Visions Unite depends on members like you to help bring the most relevant and important messages to the Visions Unite community. So if you feel this message should be seen by everyone in the group: "#{group.name}", please support this message. Likewise if you do not think this message is relevant or important to the group: "#{group.name}", in the topic: "#{topic.name}" please reject this message.

            Please log into: #{VisionsUniteWeb.Endpoint.url()}/supports/#{message.id} and make your voice heard, but don't take too long!

            Your community at group: "#{group.name}" (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id}) and all of Visions Unite appreciates your consideration.

            In Solidarity,

            Your Friends at Visions Unite
            #{VisionsUniteWeb.Endpoint.url()}/
            """
        end

      deliver_sms(user, body)
    end
  end

  @doc """
  Deliver vetting cancellation notification
  """
  def deliver_vetting_cancellation_notification(user, vetting_request) do
    {group_or_message_type, group_or_message_string} =
      if !is_nil(vetting_request.message_id) do
        message = Messages.get_message!(vetting_request.message_id)
        {"message", message.text}
      else
        group = Groups.get_group!(vetting_request.group_id)
        {"group", group.name}
      end

    subject = "[Visions Unite] #{group_or_message_type} Vetting Request Cancelled"

    body =
      """
      You have not responded to the request to vet the #{group_or_message_type}:

      >>> "#{group_or_message_string}"

      ...and are now removed from the vetting list. Nothing further is required from you.

      In Solidarity,

      Your Friends at Visions Unite
      #{VisionsUniteWeb.Endpoint.url()}/
      """

    if user.receive_emails do
      deliver_email(user, subject, body)
    end

    if user.receive_texts do
      deliver_sms(user, body)
    end
  end

  @doc """
  Deliver message support cancellation notification
  """
  def deliver_message_support_cancellation_notification(user, message) do
    subject = "[Visions Unite] Message Support Request Cancelled"

    body =
      """
      You have not responded to the request to support the message:

      >>> "#{message.text}"

      ...and are now removed from the support request list. Nothing further is required from you.

      In Solidarity,

      Your Friends at Visions Unite
      #{VisionsUniteWeb.Endpoint.url()}/
      """

    if user.receive_emails do
      deliver_email(user, subject, body)
    end

    if user.receive_texts do
      deliver_sms(user, body)
    end
  end

  @doc """
  Deliver announcement that a message has become supported.
  """
  def deliver_message_support_announcement(user, message, group, topic) do

    if user.receive_emails do
      subject = "[Visions Unite] #{topic.name} (new message)"


      body =
        """
        A new message has been supported for the group #{group.name} under the topic #{topic.name}

        #{message.text}

        You can see what else is going on in "#{group.name}" by visiting (#{VisionsUniteWeb.Endpoint.url()}/show/#{group.id})

        In Solidarity,

        Your Friends at Visions Unite
        #{VisionsUniteWeb.Endpoint.url()}/
        """

      deliver_email(user, subject, body)
    end

    # @TODO add an sms notification
    # if user.receive_texts do
    #   body = ""
    #   deliver_sms(user, body)
    # end
  end

  @doc """
  Deliver message that a group was rejected.
  """
  def deliver_group_rejected(group) do
    user = Accounts.get_user!(group.creator_id)
    deliver_email(user, "Submitted group, #{group.name}", """

    Hello #{user.email},

    Unfortunately your group, "#{group.name}", was deemed unacceptable and will never be shown to anyone.
    """)
  end

  @doc """
  Deliver message that a message was rejected.
  """
  def deliver_message_rejected(message) do
    user = Accounts.get_user!(message.author_id)
    group = Groups.get_group!(message.group_id)
    topic = Topics.get_topic!(message.topic_id)

    deliver_email(user, "Submitted message in topic #{topic.name}", """

    Hello #{user.email},

    Unfortunately your message was deemed unacceptable and will never be shown to anyone.

    Your submitted message was:

        #{message.text}

    To group "#{group.name}" in topic "#{topic.name}".

    """)
  end

end
