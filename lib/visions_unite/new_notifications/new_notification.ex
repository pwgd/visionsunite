defmodule VisionsUnite.NewNotifications.NewNotification do
  use Ecto.Schema
  import Ecto.Changeset

  alias VisionsUnite.Groups.Group
  alias VisionsUnite.Messages.Message
  alias VisionsUnite.Accounts.User

  schema "new_notifications" do
    belongs_to :group, Group
    belongs_to :message, Message
    belongs_to :user, User

    timestamps()
  end

  @doc false
  def changeset(new_notification, attrs) do
    new_notification
    |> cast(attrs, [:group_id, :message_id, :user_id])
    |> validate_required([:user_id])
    |> VisionsUnite.Utilities.validate_one_of()
    |> unique_constraint(:unique_group_notification, name: :unique_group_notification)
    |> unique_constraint(:unique_message_notification, name: :unique_message_notification)
  end
end
