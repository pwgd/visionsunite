defmodule VisionsUnite.Repo.Migrations.UserDeclineCounter do
  use Ecto.Migration
  alias VisionsUnite.Repo
  alias VisionsUnite.Accounts.User

  def up do
    alter table(:users) do
      add :declined_to_participate, :integer
    end

    flush()
    Repo.all(User)
    |> Enum.each(fn user ->
      user
      |>User.declined_to_participate_changeset(%{
        declined_to_participate: 0
      })
      |>Repo.update()
    end)
  end

  def down do
    alter table(:users) do
      remove :declined_to_participate, :integer
    end
  end
end
