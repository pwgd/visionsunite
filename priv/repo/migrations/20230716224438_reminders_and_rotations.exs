defmodule VisionsUnite.Repo.Migrations.RemindersAndRotations do
  use Ecto.Migration

  def change do
    alter table("vetting_requests") do
      add :original_request_date, :naive_datetime
      add :last_notified, :naive_datetime
    end

    alter table("support_requests") do
      add :original_request_date, :naive_datetime
      add :last_notified, :naive_datetime
    end
  end
end
