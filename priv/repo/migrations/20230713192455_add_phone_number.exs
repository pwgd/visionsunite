defmodule VisionsUnite.Repo.Migrations.AddPhoneNumber do
  use Ecto.Migration

  def change do
    alter table("users") do
      add :phone_number, :string
      add :receive_texts, :boolean
      add :receive_emails, :boolean
    end

    create index(:users, [:phone_number])
  end
end
