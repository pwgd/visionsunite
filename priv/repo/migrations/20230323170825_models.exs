defmodule VisionsUnite.Repo.Migrations.CreateModels do
  use Ecto.Migration

  def change do
    # Groups
    create table(:groups) do
      add :name, :text
      add :description, :text
      add :vetted, :boolean
      add :creator_id, references(:users, on_delete: :delete_all), null: false
      timestamps()
    end

    # Subscriptions
    create table(:subscriptions) do
      add :group_id, references(:groups, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
      timestamps()
    end

    create index(:subscriptions, [:group_id, :user_id])
    create unique_index(:subscriptions, [:group_id, :user_id], name: :unique_subscription)

    # Topics
    create table(:topics) do
      add :name, :text
      add :group_id, references(:groups, on_delete: :delete_all), null: false
      timestamps()
    end

    create index(:topics, [:group_id])

    # Messages
    create table(:messages) do
      add :text, :text
      add :vetted, :boolean
      add :fully_supported, :boolean
      add :sortition_number, :integer
      add :author_id, references(:users, on_delete: :delete_all), null: false
      add :group_id, references(:groups, on_delete: :delete_all), null: false
      add :topic_id, references(:topics, on_delete: :delete_all)
      timestamps()
    end

    create index(:messages, [:group_id])
    create index(:messages, [:topic_id])

    # Vetting Requests
    create table(:vetting_requests) do
      add :group_id, references(:groups, on_delete: :delete_all)
      add :message_id, references(:messages, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all), null: false
      timestamps()
    end

    create index(:vetting_requests, [:group_id])
    create index(:vetting_requests, [:message_id])
    create index(:vetting_requests, [:user_id])

    create unique_index(:vetting_requests, [:group_id, :user_id],
             name: :unique_group_vetting_request
           )

    create unique_index(:vetting_requests, [:message_id, :user_id],
             name: :unique_message_vetting_request
           )

    # Support Requests
    create table(:support_requests) do
      add :message_id, references(:messages, on_delete: :delete_all), null: false
      add :user_id, references(:users, on_delete: :delete_all), null: false
      timestamps()
    end

    create index(:support_requests, [:message_id])
    create index(:support_requests, [:user_id])
    create unique_index(:support_requests, [:message_id, :user_id], name: :unique_support_request)

    # Vettings
    create table(:vettings) do
      add :vet, :boolean
      add :group_id, references(:groups, on_delete: :delete_all)
      add :message_id, references(:messages, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)
      timestamps()
    end

    create index(:vettings, [:group_id])
    create index(:vettings, [:message_id])
    create index(:vettings, [:user_id])
    create unique_index(:vettings, [:user_id, :group_id], name: :unique_group_vetting)
    create unique_index(:vettings, [:user_id, :message_id], name: :unique_message_vetting)

    # Supports
    create table(:supports) do
      add :support, :boolean
      add :message_id, references(:messages, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)
      timestamps()
    end

    create index(:supports, [:message_id])
    create index(:supports, [:user_id])
    create unique_index(:supports, [:user_id, :message_id], name: :unique_support)

    # New message notifications
    create table(:new_notifications) do
      add :group_id, references(:groups, on_delete: :delete_all)
      add :message_id, references(:messages, on_delete: :delete_all)
      add :user_id, references(:users, on_delete: :delete_all)
      timestamps()
    end

    create index(:new_notifications, [:group_id])
    create index(:new_notifications, [:message_id])
    create index(:new_notifications, [:user_id])

    create unique_index(:new_notifications, [:user_id, :message_id],
             name: :unique_message_notification
           )

    create unique_index(:new_notifications, [:user_id, :group_id],
             name: :unique_group_notification
           )
  end
end
