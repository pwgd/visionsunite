defmodule VisionsUnite.Repo.Migrations.MessageDraft do
  use Ecto.Migration

  def change do
    alter table("messages") do
      add :draft, :boolean, default: true
    end
  end
end
