defmodule VisionsUnite.Repo.Migrations.TopicVetted do
  use Ecto.Migration

  def change do
    alter table("topics") do
      add :vetted, :boolean, default: false
    end
  end
end
