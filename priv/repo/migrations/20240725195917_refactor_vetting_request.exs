defmodule VisionsUnite.Repo.Migrations.RefactorVettingRequest do
  use Ecto.Migration

  def change do
    alter table(:vetting_requests) do
      add :latest_request_date, :naive_datetime
    end
  end
end
