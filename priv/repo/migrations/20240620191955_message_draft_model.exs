defmodule VisionsUnite.Repo.Migrations.MessageDraftModel do
  use Ecto.Migration

  def change do
    # Message drafts
    create table(:message_drafts) do
      add :topic_text, :text
      add :message_text, :text
      add :author_id, references(:users, on_delete: :delete_all), null: false
      add :group_id, references(:groups, on_delete: :delete_all), null: false
      timestamps()
    end

    alter table("messages") do
      remove :draft, :boolean, default: true
      modify :author_id, references(:users, on_delete: :nilify_all), null: true,
        from: {references(:users, on_delete: :delete_all), null: false}
      modify :topic_id, references(:topics, on_delete: :delete_all), null: false,
        from: {references(:topics, on_delete: :delete_all), null: true}
    end

  end
end
