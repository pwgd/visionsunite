defmodule VisionsUnite.Repo.Migrations.GroupDraftModel do
  use Ecto.Migration

  def change do
    # Group drafts
    create table(:group_drafts) do
      add :name_text, :text
      add :description_text, :text
      add :author_id, references(:users, on_delete: :delete_all), null: false
      timestamps()
    end

  end
end
