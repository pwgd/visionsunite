# Development

Visions Unite is built on [Phoenix 1.6](https://hexdocs.pm/phoenix/1.6.16/overview.html)

## Coding standards

 * Set default value for all [`System.get_env("EXAMPLE_VAR", "example default")`](https://hexdocs.pm/elixir/1.17.3/System.html#get_env/2) calls, equal to the value documented in `example.env`.


## Notification tools

If you wish to notify your users of vetting or support requests, you will need to have a Twilio (SMS) and / or SMTP service available to you.

## Running locally with Docker (recommended)

Follow the **First time setup** instructions below if you haven't already. Once that's done, start up the web and database containers:

```bash
docker compose up
```

It should be at http://localhost:4000 but check the output for "Access VisionsUniteWeb.Endpoint at"

### To run with live debugging

Including the ability to run functions from the command line.

```shell
docker compose run web iex -S mix phx.server
```

(You may need to `docker compose stop web` first; the command `make docker-restart-interactive` will do this all for you.)

### Mail

You can check for e-mail sent by the site at http://localhost:4025 (admin/admin) in Mailpit.

### Getting others changes

Most of the time you won't need a full `make build` (which starts with a `docker compose build`) and can update the project's dependencies, after `docker compose up` with:

```bash
docker compose exec web mix deps.get
```

After pulling latest code with migrations, get your database up to date with:

```bash
docker compose run web mix ecto.migrate
```

## First time setup

### Dependencies

Either Docker (recommended), or Elixir 1.15.7 and a PostgreSQL user/database.
inotify-tools is optionally used to support live-reload.

### Steps

```bash
git clone git@gitlab.com:pwgd/visionsunite.git
cp .example.env .env
make build
```

Set up the database structure with:

```bash
docker compose run web mix ecto.setup
```

NOTE: If the above does not work there, maybe it has to be done after the secret key base and salt below, but i don't think so.


Within your .env, set some additional values. The database settings are
configured to ensure proper operation in Docker.

Generate a `SIGNING_SALT` env attribute (Note the first run of `mix` will take
a bit of time to compile code.):

```bash
docker compose run web mix phx.gen.secret 32
```

Generate a `SECRET_KEY_BASE` env attribute:

```bash
docker compose run web mix phx.gen.secret 64
```

To test sending email, you will need a sendgrid API key (like SG.XXXXXXXXXXXXX)


### Alterations for local installation (Postgres / Erlang / Elixir) without Docker

It is possible to use Elixir locally, and your own Postgres database. Adjust your .env accordingly, and run the commands from the docker section above, excluding the `docker compose run web ` that prefixes all the `mix` commands.

#### Accessing the database directly

```bash
make db
```

### Dependency issues?

```bash
rm -rf deps/
rm mix.lock
docker compose run web mix deps.get
```

### Styling

For now, following Phoenix, Visions Unite uses the [Milligram minimalist CSS framework](https://milligram.io/).

### Testing error pages

To be able to see 403 and 404 pages the same way you would in the live application, edit `config/dev.exs` and change `debug_errors` from true to false:

```
  debug_errors: false,
```
