defmodule VisionsUnite.GroupsTest do
  use VisionsUnite.DataCase

  alias VisionsUnite.Groups

  describe "groups" do
    alias VisionsUnite.Groups.Group

    import VisionsUnite.GroupsFixtures

    @invalid_attrs %{description: nil, name: nil}

    test "list_groups/0 returns all groups" do
      group = group_fixture()
      assert Groups.list_all_groups() == [group]
    end

    test "get_group!/1 returns the group with given id" do
      group = group_fixture()
      assert Groups.get_group!(group.id) == group
    end

    test "create_group/1 with valid data creates a group" do
      associations = create_group_associations(nil)
      valid_attrs = %{"description" => "some description", "name" => "some name"}

      merged = Map.merge(valid_attrs, associations.associations)

      assert {:ok, %Group{} = group} = Groups.create_group(merged)
      assert group.description == "some description"
      assert group.name == "some name"
    end

    test "create_group/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Groups.create_group(@invalid_attrs)
    end

    test "update_group/2 with valid data updates the group" do
      group = group_fixture()
      update_attrs = %{"description" => "some updated description", "name" => "some updated name"}

      assert {:ok, %Group{} = group} = Groups.update_group(group, update_attrs)
      assert group.description == "some updated description"
      assert group.name == "some updated name"
    end

    test "update_group/2 with invalid data returns error changeset" do
      group = group_fixture()
      assert {:error, %Ecto.Changeset{}} = Groups.update_group(group, @invalid_attrs)
      assert group == Groups.get_group!(group.id)
    end

    test "delete_group/1 deletes the group" do
      group = group_fixture()
      assert {:ok, %Group{}} = Groups.delete_group(group)
      assert_raise Ecto.NoResultsError, fn -> Groups.get_group!(group.id) end
    end

    test "change_group/1 returns a group changeset" do
      group = group_fixture()
      assert %Ecto.Changeset{} = Groups.change_group(group)
    end
  end
end
