defmodule VisionsUnite.SupportsTest do
  use VisionsUnite.DataCase

  alias VisionsUnite.Supports

  describe "support" do
    alias VisionsUnite.Supports.Support

    import VisionsUnite.SupportsFixtures

    @valid_attrs %{"support" => true}
    @invalid_attrs %{}

    test "get_support!/1 returns the support with given id" do
      # support = support_fixture()

      # TODO support fixture creates a support, but the message needs to be vetted first

      # assert Supports.get_support!(support.id) == support
    end

    test "create_support/1 with valid data creates a support" do
      associations = create_support_associations(nil)

      merged = Map.merge(@valid_attrs, associations.associations)

      # TODO need to vet first....

      # assert {:ok, %Support{} = _support} = Supports.create_support(merged)
    end

    test "create_support/1 with invalid data returns error changeset" do
      associations = create_support_associations(nil)

      merged = Map.merge(@invalid_attrs, associations.associations)

      # TODO need to vet first....

      # assert {:error, %Ecto.Changeset{}} = Supports.create_support(merged)
    end

    test "change_support/1 returns a support changeset" do
      assert %Ecto.Changeset{} = Supports.change_support(%Support{})
    end
  end
end
