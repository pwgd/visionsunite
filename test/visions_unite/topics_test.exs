defmodule VisionsUnite.TopicsTest do
  use VisionsUnite.DataCase

  alias VisionsUnite.Topics

  describe "topics" do
    alias VisionsUnite.Topics.Topic

    import VisionsUnite.TopicsFixtures

    @invalid_attrs %{name: nil}

    test "list_topics/0 returns all topics" do
      topic = topic_fixture()
      assert Topics.list_topics() == [topic]
    end

    test "get_topic!/1 returns the topic with given id" do
      topic = topic_fixture()
      assert Topics.get_topic!(topic.id) == topic
    end

    test "create_topic/1 with valid data creates a topic" do
      associations = create_topic_associations(nil)
      valid_attrs = %{"name" => "some name"}

      merged = Map.merge(valid_attrs, associations.associations)

      assert {:ok, %Topic{} = topic} = Topics.create_topic(merged)
      assert topic.name == "some name"
    end

    test "create_topic/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Topics.create_topic(@invalid_attrs)
    end

    test "change_topic/1 returns a topic changeset" do
      topic = topic_fixture()
      assert %Ecto.Changeset{} = Topics.change_topic(topic)
    end
  end
end
