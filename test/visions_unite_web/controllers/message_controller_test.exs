defmodule VisionsUniteWeb.MessageControllerTest do
  use VisionsUniteWeb.ConnCase

  import VisionsUnite.MessagesFixtures

  @create_attrs %{"text" => "some text"}
  @invalid_attrs %{"text" => nil}

  setup :register_and_log_in_user

  describe "index" do
    test "lists my authored messages", %{conn: conn} do
      conn = VisionsUniteWeb.UserAuth.fetch_current_user(conn, nil)

      _message =
        message_fixture(
          create_message_associations_for_user(conn.assigns.current_user),
          %{"text" => "some text"}
        )

      conn = get(conn, Routes.message_path(conn, :my_authored_messages))
      assert html_response(conn, 200) =~ "<h2>My authored messages</h2>"
    end
  end

  describe "new message" do
    test "renders form", %{conn: conn} do
      associations = create_message_associations(nil)

      conn =
        get(conn, Routes.message_path(conn, :new), group_id: associations.associations["group_id"])

      assert html_response(conn, 200) =~ "<h2>New message for"
    end
  end

  describe "create message" do
    test "redirects to root when data is valid", %{conn: conn} do
      associations = create_message_associations(nil)

      merged = Map.merge(@create_attrs, associations.associations)

      conn = post(conn, Routes.message_path(conn, :create), message: merged)

      assert redirected_to(conn) == Routes.group_path(conn, :show_group, merged["group_id"])
    end

    test "renders errors when data is invalid", %{conn: conn} do
      associations = create_message_associations(nil)

      merged = Map.merge(@invalid_attrs, associations.associations)

      conn = post(conn, Routes.message_path(conn, :create), message: merged)
      assert html_response(conn, 200) =~ "<h2>New message for"
    end
  end
end
