defmodule VisionsUniteWeb.GroupControllerTest do
  use VisionsUniteWeb.ConnCase

  import VisionsUnite.GroupsFixtures

  @create_attrs %{"description" => "some description", "name" => "some name"}
  @update_attrs %{"description" => "some updated description", "name" => "some updated name"}
  @invalid_attrs %{"description" => nil, "name" => nil}

  setup :register_and_log_in_user

  describe "index" do
    test "lists all groups", %{conn: conn} do
      conn = get(conn, Routes.group_path(conn, :all_groups))
      assert html_response(conn, 200) =~ "Groups"
    end
  end

  describe "new group" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.group_path(conn, :new))
      assert html_response(conn, 200) =~ "New Group"
    end
  end

  describe "create group" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.group_path(conn, :create), group: @create_attrs)

      assert redirected_to(conn) == Routes.group_path(conn, :all_groups)

      conn = get(conn, Routes.group_path(conn, :all_groups))

      assert html_response(conn, 200) =~ "Groups"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.group_path(conn, :create), group: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Group"
    end
  end

  defp create_group(_) do
    group = group_fixture()
    %{group: group}
  end
end
