defmodule VisionsUniteWeb.PageControllerTest do
  use VisionsUniteWeb.ConnCase

  setup :register_and_log_in_user

  describe "homepage" do
    test "GET /", %{conn: conn} do
      conn = get(conn, "/")
      assert html_response(conn, 200) =~ "Groups"
    end
  end
end
