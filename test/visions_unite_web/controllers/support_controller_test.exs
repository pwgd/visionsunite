defmodule VisionsUniteWeb.SupportControllerTest do
  use VisionsUniteWeb.ConnCase

  import VisionsUnite.MessagesFixtures

  @create_attrs %{"support" => true}

  setup :register_and_log_in_user

  describe "create support" do
    test "redirects to group index when only one support request", %{conn: conn} do
      conn = VisionsUniteWeb.UserAuth.fetch_current_user(conn, nil)

      message =
        message_fixture(
          create_message_associations_for_user(conn.assigns.current_user),
          %{"text" => "some text"}
        )

      merged =
        Map.merge(@create_attrs, %{
          "message_id" => message.id,
          "user_id" => conn.assigns.current_user.id
        })

      # we want conn.assigns.current_user.id to be the user that gets support requested of them
      #   and thus will have that support request deleted when giving support

      # TODO need to vet messages before support stuff

      conn = post(conn, Routes.support_path(conn, :create), support: merged)
      assert redirected_to(conn) == Routes.group_path(conn, :index)
    end

    test "redirects to support requests when more than one support request", %{conn: conn} do
      conn = VisionsUniteWeb.UserAuth.fetch_current_user(conn, nil)

      message1 =
        message_fixture(
          create_message_associations_for_user(conn.assigns.current_user),
          %{"text" => "some text"}
        )

      # although unused, this 2nd message ensures the user will have multiple support requests
      #   and so, when creating a support for one message, there will still be one more, and
      #   we can test that it returns to the support requests page
      _message2 =
        message_fixture(
          create_message_associations_for_user(conn.assigns.current_user),
          %{"text" => "some text"}
        )

      merged =
        Map.merge(@create_attrs, %{
          "message_id" => message1.id,
          "user_id" => conn.assigns.current_user.id
        })

      # TODO need to vet messages before support stuff

      conn = post(conn, Routes.support_path(conn, :create), support: merged)
      assert redirected_to(conn) == Routes.group_path(conn, :index)
      conn = get(conn, Routes.group_path(conn, :index))
      assert redirected_to(conn) == Routes.support_request_path(conn, :requesting_support)
    end
  end
end
