defmodule VisionsUnite.SupportsFixtures do
  import VisionsUnite.MessagesFixtures

  alias VisionsUnite.Subscriptions

  @moduledoc """
  This module defines test helpers for creating
  entities via the `VisionsUnite.Supports` context.
  """

  @doc """
  Generate a support's associations.
  """
  def create_support_associations(_) do
    message = message_fixture()

    # By creating a message fixture, 2 users have been created as well,
    #   the author of the message, and a user subscribed to the group
    #   that the message belongs to. We use this 2nd user as the user
    #   that will give the support to the message.

    group_subscriptions = Subscriptions.list_subscriptions_for_group(message.group_id)

    user_id =
      group_subscriptions
      |> Enum.map(& &1.user_id)
      |> Enum.find(fn user_id ->
        user_id !== message.author_id
      end)

    %{
      associations: %{
        "user_id" => user_id,
        "message_id" => message.id
      }
    }
  end

  @doc """
  Generate a support.
  """
  def support_fixture(
        associations \\ create_support_associations(nil),
        attrs \\ %{
          "support" => true
        }
      ) do
    attrs = Map.merge(attrs, associations.associations)

    {:ok, support} = VisionsUnite.Supports.create_support(attrs)

    support
  end
end
