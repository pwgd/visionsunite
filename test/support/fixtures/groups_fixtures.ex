defmodule VisionsUnite.GroupsFixtures do
  import VisionsUnite.AccountsFixtures

  @moduledoc """
  This module defines test helpers for creating
  entities via the `VisionsUnite.Groups` context.
  """

  @doc """
  Generate a group's associations.
  """
  def create_group_associations(_) do
    user = user_fixture()

    %{
      associations: %{
        "creator_id" => user.id
      }
    }
  end

  @doc """
  Generate a group.
  """
  def group_fixture(
        associations \\ create_group_associations(nil),
        attrs \\ %{
          "description" => "some description",
          "name" => "some name"
        }
      ) do
    attrs = Map.merge(attrs, associations.associations)

    {:ok, group} = VisionsUnite.Groups.create_group_and_request_vetting(attrs)

    group
  end
end
