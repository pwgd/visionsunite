defmodule VisionsUnite.MessagesFixtures do
  import VisionsUnite.AccountsFixtures
  import VisionsUnite.GroupsFixtures
  import VisionsUnite.TopicsFixtures

  alias VisionsUnite.Subscriptions

  @moduledoc """
  This module defines test helpers for creating
  entities via the `VisionsUnite.Messages` context.
  """

  @doc """
  Generate a message's associations.
  """
  def create_message_associations(_) do
    user = user_fixture()
    group = group_fixture()
    topic = topic_fixture()

    # subscribe user to the group
    Subscriptions.create_subscription(%{
      "user_id" => user.id,
      "group_id" => group.id
    })

    # second user for support request
    user2 = user_fixture()

    # subscribe 2nd user to this group
    Subscriptions.create_subscription(%{
      "group_id" => group.id,
      "user_id" => user2.id
    })

    %{
      associations: %{
        "author_id" => user.id,
        "group_id" => group.id,
        "topic_id" => topic.id
      }
    }
  end

  @doc """
  Generate a message's associations for a particular user.
  """
  def create_message_associations_for_user(user2) do
    user = user_fixture()
    group = group_fixture()
    topic = topic_fixture()

    # subscribe user to the group
    Subscriptions.create_subscription(%{
      "user_id" => user.id,
      "group_id" => group.id
    })

    # subscribe 2nd user to this group
    Subscriptions.create_subscription(%{
      "group_id" => group.id,
      "user_id" => user2.id
    })

    %{
      associations: %{
        "author_id" => user.id,
        "group_id" => group.id,
        "topic_id" => topic.id
      }
    }
  end

  @doc """
  Generate a message.
  """
  def message_fixture(
        associations \\ create_message_associations(nil),
        attrs \\ %{
          "text" => "some text"
        }
      ) do
    attrs = Map.merge(attrs, associations.associations)

    {:ok, message} = VisionsUnite.Messages.finalize_message_and_request_vetting(attrs)

    message
  end
end
