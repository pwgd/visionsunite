defmodule VisionsUnite.TopicsFixtures do
  import VisionsUnite.GroupsFixtures

  @moduledoc """
  This module defines test helpers for creating
  entities via the `VisionsUnite.Topics` context.
  """

  @doc """
  Generate a topic's associations.
  """
  def create_topic_associations(_) do
    group = group_fixture()

    %{
      associations: %{
        "group_id" => group.id
      }
    }
  end

  @doc """
  Generate a topic.
  """
  def topic_fixture(
        associations \\ create_topic_associations(nil),
        attrs \\ %{
          "name" => "some name"
        }
      ) do
    attrs = Map.merge(attrs, associations.associations)

    {:ok, topic} = VisionsUnite.Topics.create_topic(attrs)

    topic
  end
end
