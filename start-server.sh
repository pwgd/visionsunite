#!/usr/bin/env bash
set -e
mix ecto.setup
mix ecto.migrate
mix phx.server
