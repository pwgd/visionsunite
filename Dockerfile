#docker/dockerfile:1.2
ARG NODE_VERSION=12
ARG ELIXIR_VERSION=1.15.7

# ENV APP_HOME /app
# ENV .env
FROM node:${NODE_VERSION} as assets
ENV APP_HOME /app
WORKDIR $APP_HOME
COPY ./assets .

RUN npm install --prefix ./assets


FROM elixir:${ELIXIR_VERSION} as base

ENV PHX_VERSION 1.7.10

RUN set -xe &&\
    mix local.hex --force && \
    mix local.rebar --force && \
    mix archive.install hex phx_new $PHX_VERSION --force

ENV APP_HOME /app
WORKDIR $APP_HOME
COPY . .
COPY --link --from=assets $APP_HOME/assets ./assets

ENTRYPOINT ["/app/entrypoint.sh"]


FROM base as prod

ENV MIX_ENV prod
ENV NODE_ENV production
RUN mix deps.get --only prod
RUN mix compile
RUN mix assets.deploy

CMD ["./start-server.sh"]

FROM base as dev
ARG NODE_VERSION=12

RUN curl https://raw.githubusercontent.com/creationix/nvm/master/install.sh | bash

RUN mix deps.get
RUN mix compile
RUN mix assets.deploy

RUN . /root/.nvm/nvm.sh && \
    nvm install $NODE_VERSION && \
    npm install --prefix ./assets && \
    mix assets.deploy
CMD ["iex", "-S", "mix", "phx.server"]
