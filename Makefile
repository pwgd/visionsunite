SHELL := bash
.ONESHELL:
.SHELLFLAGS := -eu -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules
# Credits: https://tech.davis-hansson.com/p/make/

.PHONY: build clean docker-up docker-run docker-restart-interactive logs config db

config:
	docker buildx &>/dev/null && echo "buildx is enabled/available" || echo "Experimental docker features must be enabled with export DOCKER_CLI_EXPERIMENTAL=enabled (or else install buildx)"

build:
	docker compose build

clean:
	rm mix.lock
	rm -rf deps/

docker-up:
	docker compose up -d

docker-restart-interactive:
	docker compose stop web
	docker compose run web iex -S mix phx.server

docker-run:
	docker compose run web $@

logs:
	docker compose logs $(service)

db:
	docker compose exec db psql -h localhost -U postgres -d visions_unite_dev
