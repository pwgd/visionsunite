defmodule VisionsUnite.MixProject do
  use Mix.Project

  def project do
    [
      app: :visions_unite,
      version: "0.1.0",
      elixir: "~> 1.15.7",
      elixirc_paths: elixirc_paths(Mix.env()),
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {VisionsUnite.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:bcrypt_elixir, "~> 3.1.0"},
      {:phoenix, "~> 1.7.10"},
      {:phoenix_ecto, "~> 4.4.3"},
      {:ecto_sql, "~> 3.6"},
      {:eqrcode, "~> 0.1.10"},
      {:postgrex, ">= 0.0.0"},
      {:phoenix_html, "~> 3.3.3"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.20.1"},
      {:phoenix_view, "~> 2.0"},
      {:floki, ">= 0.35.2", only: :test},
      {:phoenix_live_dashboard, "~> 0.8.2"},
      {:esbuild, "~> 0.8.1", runtime: Mix.env() == :dev},
      {:swoosh, "~> 1.14.2"},
      {:gen_smtp, "~> 1.1.0"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      {:gettext, "~> 0.23.1"},
      {:jason, "~> 1.4.1"},
      {:plug_cowboy, "~> 2.5"},
      {:statistics, "~> 0.6.2"},
      {:finch, "~> 0.14.0"},
      {:earmark, "~> 1.4.46"},
      {:linkify, "~> 0.5.3"},
      {:html_sanitize_ex, "~> 1.4.3"},
      {:ex_twilio, "~> 0.9.1"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["deps.get", "ecto.setup", "assets.setup", "assets.build"],
      "ecto.setup": ["ecto.create", "ecto.migrate", "run priv/repo/seeds.exs"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      test: ["ecto.create --quiet", "ecto.migrate --quiet", "test"],
      "assets.setup": ["esbuild.install --if-missing"],
      "assets.build": ["esbuild default"],
      "assets.deploy": ["esbuild default --minify", "phx.digest"]
    ]
  end
end
